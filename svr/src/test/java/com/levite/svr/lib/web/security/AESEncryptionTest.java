package com.levite.svr.lib.web.security;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.levite.svr.external.web.security.AESEncryption;

public class AESEncryptionTest 
{
    private AESEncryption encryption;

    private static final String SECRET = "I AM BIG SECRET";

    @BeforeEach
    public void init() throws Exception
    {
        this.encryption = new AESEncryption(SECRET);
    }

    @Test
    public void testEncryptAndDecrypt()
    {
        final String dontTellAnyone = "I'm spiderman...";
        final String encrypted = this.encryption.encrypt(dontTellAnyone);
        assertEquals(dontTellAnyone, this.encryption.decrypt(encrypted));
    }

    @Test
    public void testEncryptAndDecryptLater() throws Exception
    {
        final String dontTellAnyone = "I'm spiderman...";
        final String encrypted = this.encryption.encrypt(dontTellAnyone);
        assertEquals(dontTellAnyone, new AESEncryption(SECRET).decrypt(encrypted));
    }
}
