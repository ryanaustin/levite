package com.levite.svr.integration;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.levite.svr.internal.data.cache.Cache;
import com.levite.svr.internal.data.mutations.TxNameAnonymizer;
import com.levite.svr.internal.structs.Tx;
import com.levite.svr.internal.structs.TxContext;
import com.levite.svr.internal.structs.TxKey;
import com.levite.svr.testutil.TestData;
import com.levite.svr.testutil.TestTxContext;

import static com.levite.svr.util.CastingUtil.cast;

public class TxMutatorTests {

    @BeforeEach 
    public void init() {
        TestTxContext.init();
        TestData.loadIntoCache();
        if (Cache.MEMORY.isEmpty()) {
            throw new IllegalStateException(
                    "Cache was not loaded with test data");
        }
    }

    @Nested
    class TxNameAnonymizerTests {

        private Tx tx;
        private TxNameAnonymizer anonymizer = new TxNameAnonymizer();

        @BeforeEach
        public void setupAnonymizer() {
            tx = cast(Cache.MEMORY.values().iterator().next())
                    .to(Tx.class)
                    .orThrow(() -> new IllegalStateException(
                            "unable to get a TX to test with"));
        }

        @Test
        void givenTxWithName_WhenAnonymized_ReplacesName() {

            final Tx mutated = anonymizer.clean(tx);
            mutated.save();
            System.out.println("MUTATED: " + TxContext.get().getMapper().toJson(mutated.getValues()));

            assertTrue(mutated.has(TxKey.ANON_NAME));
            assertFalse(mutated.has(TxKey.NAME));
        }

    }
    
}
