package com.levite.svr.integration;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.levite.svr.internal.data.cache.Cache;
import com.levite.svr.internal.data.loading.CacheDataLoader;
import com.levite.svr.internal.data.loading.DataLoader.DataType;
import com.levite.svr.testutil.TestData;
import com.levite.svr.testutil.TestTxContext;

public class TxLoadingTests {
    

    @BeforeEach
    public void init()
    {
        Cache.MEMORY.clear();
        TestTxContext.init();
    }

    @Nested
    class CacheDataLoadingTests {

        private final CacheDataLoader loader = new CacheDataLoader();

        @Test
        void givenTestData_V1_WhenLoad_PopulatesCache() {
            
            final InputStream testData = TestData.load();

            loader.load(DataType.TX, testData);
            
            assertEquals(6, Cache.MEMORY.size());
        }
    }
}
