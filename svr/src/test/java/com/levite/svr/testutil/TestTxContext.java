package com.levite.svr.testutil;

import com.levite.svr.external.jackson.JacksonMapper;
import com.levite.svr.internal.data.mutations.TxMutator;
import com.levite.svr.internal.structs.Permission;
import com.levite.svr.internal.structs.TxContext;
import com.levite.svr.util.Mapper;
import com.levite.svr.util.ThreadCtx;

import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of {@link TxContext} to be used in the test environment
 */
public class TestTxContext implements TxContext {
    
    public static void init() {
        destroy();
        ThreadCtx.set(new TestTxContext());
    }

    public static void destroy() {
        ThreadCtx.clear();
    }

    @Override
    public Principal getPrincipal() {
        throw new UnsupportedOperationException(
            "Unimplemented method 'getPrincipal'");
    }

    @Override
    public Collection<Permission> getPermissions() {
        throw new UnsupportedOperationException(
            "Unimplemented method 'getPermissions'");
    }

    @Override
    public Object getRequest() {
        throw new UnsupportedOperationException(
            "Unimplemented method 'getRequest'");
    }

    @Override
    public Optional<Object> getTransaction() {
        return Optional.empty();
    }

    @Override
    public Mapper getMapper() {
        return new JacksonMapper();
    }

    @Override
    public List<TxMutator> getMutators() {
        return List.of();
    }


}
