package com.levite.svr.testutil;

import java.io.InputStream;

import com.levite.svr.internal.data.loading.CacheDataLoader;
import com.levite.svr.internal.data.loading.DataLoader.DataType;
import com.levite.svr.util.aop.ServerClassLoader;

public class TestData {
    
    public static InputStream load() { 
        
        return ServerClassLoader.get()
            .getResourceAsStream("TxTestData_V1.json");
    }

    public static void loadIntoCache() {
        new CacheDataLoader().load(DataType.TX, TestData.load());
    }
}
