package com.levite.svr.util.aop;

public class ServerClassLoader {
    
    // hide
    private ServerClassLoader() {}

    public static ClassLoader get() {
        return ServerClassLoader.class.getClassLoader();
    }

}
