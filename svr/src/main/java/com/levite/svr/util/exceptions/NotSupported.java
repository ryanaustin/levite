package com.levite.svr.util.exceptions;

import lombok.Getter;

/**
 * Indicates that something is unsupported and requires an error code that can
 * be mapped to exactly why this is.
 */
@Getter
public class NotSupported extends UnsupportedOperationException {
    private final String errorCode;

    public NotSupported(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public NotSupported(String message, Throwable cause, String errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }
}
