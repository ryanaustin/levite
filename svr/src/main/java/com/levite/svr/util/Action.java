package com.levite.svr.util;

public interface Action {
    void exec();
}
