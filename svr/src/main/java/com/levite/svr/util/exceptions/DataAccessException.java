package com.levite.svr.util.exceptions;

public class DataAccessException extends RuntimeException {
    public DataAccessException(String s, Exception cause) {
        super(s, cause);
    }
}
