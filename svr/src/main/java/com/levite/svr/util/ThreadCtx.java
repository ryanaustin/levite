package com.levite.svr.util;

import static com.levite.svr.util.CastingUtil.cast;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public interface ThreadCtx {

    ThreadLocal<Map<Class<?>, Object>> CTX =
        ThreadLocal.withInitial(ConcurrentHashMap::new);

    static <C> void set(C ctx) {
        CTX.get().put(ctx.getClass(), ctx);
    }
    
    
    static <C> InContext<C> inContext(C ctx) {
        return new InContext<>(ctx);
    }

    static <C> C get(Class<C> clazz) {

        return cast(
                CTX.get().getOrDefault(clazz, CTX.get().entrySet()
                    .stream()
                    .filter(entry -> clazz.isAssignableFrom(entry.getKey()))
                    .map(Map.Entry::getValue)
                    .findFirst()
                    .orElse(null)))
            .to(clazz)
            .orReturn(null);
    }

    final class InContext<C> {

        private final C ctx;

        private InContext(C ctx) {
            this.ctx = ctx;
        }

        public <R> R doExecute(ReturnableAction<R> action) {

            CTX.get().put(ctx.getClass(), ctx);

            R returnValue;
            
            try {
                returnValue = action.exec();        
            }
            catch (Exception e) {

                CTX.remove();
                throw e;
            }
            CTX.remove();
            return returnValue;
        }

        public void doExecute(Action action) {

            CTX.get().put(ctx.getClass(), ctx);

            try {
                action.exec();
            }
            catch (Exception e) {

                CTX.remove();
                throw e;
            }
            CTX.remove();
        }

    }

    static void clear() {
        CTX.remove();
    }
}
