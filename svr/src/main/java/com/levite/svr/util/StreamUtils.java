package com.levite.svr.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class StreamUtils {
    
    // hide
    private StreamUtils() {}

    public static String toJson(InputStream is) {
        return new BufferedReader(new InputStreamReader(
                        is, StandardCharsets.UTF_8)).lines()
                .collect(Collectors.joining("\n"));
    }

}
