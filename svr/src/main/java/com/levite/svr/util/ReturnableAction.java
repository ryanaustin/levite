package com.levite.svr.util;

public interface ReturnableAction<R> {
    R exec();
}
