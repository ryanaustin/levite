package com.levite.svr.util;

import java.util.stream.Stream;

import com.levite.svr.internal.structs.Tx;
import com.levite.svr.internal.structs.TxKey;

import lombok.Getter;

@Getter
public class NormalizedName {
    private final String actual;
    private final String normalized;

    private NormalizedName(String actualName, String normalized) {
        this.actual = actualName;
        this.normalized = normalized;
    }

    public static NormalizedName from(String actualName) {
        return new NormalizedName(actualName, normalize(actualName));
    }

    /**
     * Creates and returns a NormalizedName if the given {@link Tx} has a 
     * {@link TxKey#NAME} value. Otherwise, this method returns null.
     */
    public static NormalizedName from(Tx tx) {
        final Object actualName = tx.get(TxKey.NAME);
        if (actualName == null) return null;
        final String asString = actualName.toString();
        return new NormalizedName(asString, normalize(asString));
    }

    /**
     * Normalizes an actual name so that formatting, white spaces, and 
     * capitalization can be ignored for the purposes of database filtering
     * and searching. 
     * 
     * Note: A normalized name cannot be converted back to the actual name.
     */
    public static String normalize(String actualName) {
        return String.join("", 
                Stream.of(actualName.split("\\s+"))
                        .map(s -> s.toLowerCase())
                        .toArray(String[]::new));
    }
}
