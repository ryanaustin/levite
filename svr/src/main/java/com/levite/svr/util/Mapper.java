package com.levite.svr.util;

import java.util.Collection;
import java.util.Optional;

import com.levite.svr.internal.structs.KeyValue;
import com.levite.svr.internal.structs.NamedLists;
import com.levite.svr.internal.structs.Tx;

public interface Mapper {

    /**
     * From the given KeyValue pair, procure an object of the given type if it
     * is within this mapper's capabilities to do so.
     */
    <T> Optional<T> procure(KeyValue keyValue, Class<T> type);

    /**
     * Parse the given json string into the an object defined by {@param clazz
     * the given type}, then return the object.
     */
    <E> E readObject(String json, Class<E> clazz);

    /**
     * Parse the given json string into a collection of Tx's.
     */
    Collection<Tx> readTxs(String json);

    <T> NamedLists<T> readNamedLists(String json, Class<T> typeOfList);

    String toJson(Object obj);
}
