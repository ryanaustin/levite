package com.levite.svr.util;

import java.util.function.Function;

public final class CastingUtil {
    @SuppressWarnings("unchecked")
    public static final class Cast<O> {
        private final O orig;
        private Object casted;


        private Cast(O orig) {
            this.orig = orig;
        }

        public <C> Cast<O> to(Class<C> castTo) {
            if (castTo.isInstance(orig)) {
                this.casted = castTo.cast(orig);
            }
            return this;
        }

        public <T, C extends Class<T>> Cast<O> toClass(Class<T> objectClass) {
            if (orig instanceof Class<?> && objectClass.isAssignableFrom((Class<?>) orig)) {
                this.casted = orig;
            }
            return this;
        }
        public <C> C orElse(Function<O, C> funct) {
            if (casted != null) return (C) casted;

            if (funct == null) return null;
            return funct.apply(orig);
        }

        public <C> C orReturn(ReturnableAction<C> returnableAction) {
            if (casted != null) return (C) casted;

            if (returnableAction == null) return null;
            return returnableAction.exec();
        }

        public <R extends RuntimeException, C> C orThrow(
                ThrowableAction<R> throwableAction) {
            if (casted != null) return (C) casted;

            throw throwableAction.exec();
        }

    }

    public static <O, C> Cast<O> cast(O obj) {
        return new Cast<>(obj);
    }


}
