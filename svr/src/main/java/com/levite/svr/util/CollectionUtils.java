package com.levite.svr.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Stream;

import com.levite.svr.internal.structs.KeyValue;

public class CollectionUtils {

    private CollectionUtils() {}


    public static <R> Stream<R> streamOf(Iterator<R> iterator)
    {
        final Collection<R> c = new ArrayList<>();
        while (iterator.hasNext()) c.add(iterator.next());
        return c.stream();
    }

    public static Map<Object, Object> toMap(Collection<KeyValue> keyValuePairs) {
        return keyValuePairs.stream()
            .collect(
                TreeMap::new,
                (m, kv) -> m.put(kv.getKey(), kv.getValue()),
                TreeMap::putAll);
    }

}
