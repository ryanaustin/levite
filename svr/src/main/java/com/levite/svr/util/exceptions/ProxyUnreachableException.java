package com.levite.svr.util.exceptions;

public class ProxyUnreachableException extends RuntimeException {
    public ProxyUnreachableException(String s) {
        super(s);
    }
}
