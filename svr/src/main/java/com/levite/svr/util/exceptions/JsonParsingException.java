package com.levite.svr.util.exceptions;

public class JsonParsingException extends RuntimeException {
    
    public JsonParsingException(String s, Exception cause) {
        super(s, cause);
    }
}
