package com.levite.svr.util.aop;

public @interface Contextualize {

    Class<?> [] value() default {};

}