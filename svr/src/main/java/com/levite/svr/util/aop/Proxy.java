package com.levite.svr.util.aop;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import com.levite.svr.internal.structs.Base;

@Retention(RetentionPolicy.RUNTIME)
public @interface Proxy {

    Class<?> value() default Base.class;

}