package com.levite.svr.util;

import com.levite.svr.internal.structs.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

import static com.levite.svr.util.CastingUtil.cast;

public class SQL {
    public static final String SELECT_ALL_FROM = "SELECT * FROM";
    public static final String INSERT_INTO = "INSERT INTO";
    public static final String VALUES = "VALUES";
    public static final String NULL = "null";
    public static final String WHERE = "WHERE";
    public static final String ON_CONFLICT = "ON CONFLICT";
    public static final String DO_UPDATE_SET = "DO UPDATE SET";
    public static final String IS_NOT_NULL = "IS NOT NULL";
    public static final String EXCLUDED = "EXCLUDED";
    public static final String _EQ_ = "=";
    public static final String _S_ = " ";
    public static final String _END_ = ";";
    public static final String IN = "IN";
    public static final String CASE = "CASE";
    public static final String WHEN = "WHEN";
    public static final String THEN = "THEN";
    public static final String ELSE = "ELSE";
    public static final String END = "END";
    public static final String AND = "AND";
    public static final String OR = "OR";
    public static final Map<Filter.FilterType, String> FILTER_TO_OP =
            Map.of(Filter.FilterType.EQ, "=",
                    Filter.FilterType.GT, ">",
                    Filter.FilterType.LT, "<",
                    Filter.FilterType.GTE, ">=",
                    Filter.FilterType.LTE, "<=",
                    Filter.FilterType.LIKE, "LIKE");

    /**
     * Convert the given list of key value pairs to a schema map.
     */
    public static Map<Object, Object> toSchemaMap(List<KeyValue> keyValues) {
        return keyValues.stream().collect(
                HashMap::new,
                (m, e) -> m.put(e.getKey(), e.getValue()),
                HashMap::putAll);
    }

    /**
     * Appends the {@code VALUES (...)} portion of an SQL statement. There is no
     * leading or trailing spaces appended by this function.
     *
     * @param sqlStatement the sql statement to append to.
     * @param columns the columns that should be updated, which must match the schema
     * @param schema a map of the schema where the key corresponds to the column when
     * toString() is resolved and the value corresponds to the expected Java class type.
     * @param valuesToAdd a collection of {@link Valuable entries} that each hold values
     * for the columns in the schema.
     */
    public static <T extends Valuable> void appendValues(StringBuilder sqlStatement,
            String[] columns, Map<Object, Object> schema,
            Collection<T> valuesToAdd) {

        sqlStatement.append(VALUES).append(_S_).append("(");
        int index = valuesToAdd.size();
        for (Valuable valueHolder : valuesToAdd) {
            index--;
            sqlStatement.append("(");

            final String[] stringValues = new String[columns.length];

            for (int i = 0; i < stringValues.length; i++) {
                final String column = columns[i];
                Object value = valueHolder.get(column);

                if (value != null && column.equals(TxKey.ID) &&
                        valueHolder.toString().isEmpty()) {
                    value = UUID.randomUUID().toString();
                }
                Class<?> expectedType =
                        cast(schema.get(column)).toClass(Object.class)
                                .orThrow(() -> new IllegalStateException(
                                        "schema values must all be class types, instead schema was: " +
                                                schema));

                if (value == null) {
                    stringValues[i] = NULL;
                } else if (String.class.equals(expectedType) ||
                        LocalDate.class.equals(expectedType) ||
                        LocalTime.class.equals(expectedType)) {
                    stringValues[i] = "'" + valueHolder + "'";
                } else {
                    stringValues[i] = valueHolder.toString();
                }
            }
            sqlStatement.append(String.join(",", stringValues)).append(")");
            if (index != 0) {
                sqlStatement.append(",");
            }
        }
        sqlStatement.append(")");
    }
}
