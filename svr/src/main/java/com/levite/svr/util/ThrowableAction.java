package com.levite.svr.util;

public interface ThrowableAction<R> {
    R exec();
}
