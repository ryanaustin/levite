package com.levite.svr.util.exceptions;

public class IllegalInput extends RuntimeException
{

    public IllegalInput()
    {
        super();
    }
    
    public IllegalInput(String message)
    {
        super(message);
    }
}
