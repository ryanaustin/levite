package com.levite.svr.internal.structs;

public enum TxType {
    GIFT("gift", 1),
    EXPENSE("expense", -1),
    CHECK_DEPOSIT("check_deposit", 1),
    CASH_DEPOSIT("cash_deposit", 1),
    CHECK_WITHDRAWAL("check_withdrawal", -1),
    CASH_WITHDRAWAL("cash_withdrawal", -1),
    SALE("sale", 1),
    OTHER("other", 1);

    private final String type;
    private final int sign;

    TxType(String type, int sign) {
        this.type = type;
        this.sign = sign;
    }

    @Override
    public String toString() {
        return type;
    }
}
