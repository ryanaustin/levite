package com.levite.svr.internal.structs;

import com.levite.svr.util.CollectionUtils;

import java.util.*;

import static com.levite.svr.internal.structs.UniqueId.ID_NAME;
import static com.levite.svr.internal.structs.UniqueId.TENANT_ID_NAME;


/**
 * Mutable Transaction (Tx) data structure.
 */
public interface Tx extends Tenable, Valuable {

    Collection<KeyValue> getValues();
    
    default void put(Object key, Object value)
    {
        put(Collections.singletonList(KeyValue.of(key, value)));
    }
    
    void put(Collection<KeyValue> keyValues);
    
    void remove(Collection<Object> keys);

    @Override
    default UniqueId getUniqueId() {
        return getUniqueId(this.getValues());
    }

    default <T> Optional<T> get(Class<T> type) {
        Objects.requireNonNull(type);
        return getValues()
                .stream()
                .map(KeyValue::getValue)
                .filter(type::isInstance)
                .map(type::cast)
                .findFirst();
    }
    
    default Map<Object, Object> asMap() {
        return CollectionUtils.toMap(getValues());
    }

    default Object get(Object key) {
        Objects.requireNonNull(key);
        return getValues()
            .stream()
            .filter(kv -> key.equals(kv.getKey()))
            .findFirst()
            .orElse(null);
    }

    default boolean has(String key) {
        return get(key) != null;
    }
    
    static UniqueId getUniqueId(Collection<KeyValue> keyValuePairs) {
        return new UniqueId(
            keyValuePairs
                .stream()
                .filter(kv -> kv.getKey() != null && 
                    kv.getKey().equals(TENANT_ID_NAME))
                .map(Object::toString)
                .findFirst()
                .orElseThrow(() ->
                    new IllegalArgumentException(String.format(
                        "collection of key-values <%s> does not contain " + 
                        "a <%s>", keyValuePairs, TENANT_ID_NAME))),
            keyValuePairs
                .stream()
                .filter(kv -> kv.getKey() != null && 
                    kv.getKey().equals(ID_NAME))
                .map(KeyValue::getValue)
                .map(Object::toString)
                .findFirst()
                .orElseThrow(() ->
                    new IllegalArgumentException(String.format(
                        "collection of key-values <%s> does not contain " + 
                        "a <%s>", keyValuePairs, ID_NAME))
            ));
    }

    Tx save();
}
