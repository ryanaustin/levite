package com.levite.svr.internal.structs;


import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

@Getter
public class View {
    public static View ALL = new View();

    private final Map<String, Class<?>> elements = new HashMap<>();

}
