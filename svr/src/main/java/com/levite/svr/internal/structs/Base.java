package com.levite.svr.internal.structs;

public interface Base {

    default <T> T noImpl(String methodName) {
        final String explanationForError = String.format(
            "method <%s> not implemented by <%s>",
            methodName,
            this.getClass().getSimpleName());

        throw new UnsupportedOperationException(explanationForError);
    }

}
