package com.levite.svr.internal.data.accessors.impl;

import com.levite.svr.internal.data.accessors.TxAccessor;
import com.levite.svr.internal.data.db.JdbcTxDb;
import com.levite.svr.internal.data.db.TxDb;
import com.levite.svr.internal.structs.Filter;
import com.levite.svr.internal.structs.Filter.FilterType;
import com.levite.svr.internal.structs.Tx;
import com.levite.svr.internal.structs.TxKey;
import com.levite.svr.util.SQL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static com.levite.svr.util.SQL.*;

/**
 * SQL abstract implementation of {@link TxAccessor} that must be hooked up
 * to an SQL compatible implementation of {@link TxDb}.
 */
public abstract class SqlTxDbAccessor implements TxAccessor {
    protected static final Logger logger =
            LoggerFactory.getLogger(SqlTxDbAccessor.class);

    protected abstract TxDb getDb();

    protected Tx getSchema() {
        return getDb().getSchema();
    }

    @Override
    public Collection<Tx> get(List<Filter> filters) {

        final StringBuilder sqlStatement = new StringBuilder();

        sqlStatement.append(SELECT_ALL_FROM).append(_S_).append(
                JdbcTxDb.TABLE_NAME);

        if (!filters.isEmpty()) {
            for (Filter moreFilter : filters) {
                sqlStatement.append(_S_).append(AND);
                applyFilter(sqlStatement, moreFilter);
            }
        }
        sqlStatement.append(_END_);
        return getDb().execute(sqlStatement.toString());
    }

    /**
     * <p>Inserts or updates the given transactions.</p>
     * <p>
     * When updating, only values that are not null updated and each given
     * {@link Tx tx} must match the {@link #getSchema() current schema}. Any
     * values that are not within the schema will be ignored.
     */
    @Override
    public void put(Collection<Tx> txs) {
        if (txs.isEmpty()) {
            logger.warn(String.format(
                    "nothing to put into table <%s> because collection is empty",
                    JdbcTxDb.TABLE_NAME));
            return;
        }

        final StringBuilder sqlStatement = new StringBuilder();
        sqlStatement.append(INSERT_INTO).append(_S_).append(JdbcTxDb.TABLE_NAME)
                .append("(");
        final Tx schema = getSchema();
        final Map<Object, Object> schemaMap = schema.asMap();
        final String[] columns =
                schemaMap.keySet().stream().map(Object::toString)
                        .toArray(String[]::new);
        sqlStatement.append(String.join(",", columns)).append(")").append(_S_);

        // appends the VALUES (...)
        SQL.appendValues(sqlStatement, columns, schemaMap, txs);

        // this all handles updating existing tx's
        sqlStatement.append(_S_).append(ON_CONFLICT).append(_S_).append("(")
                .append(TxKey.ID).append(",").append(TxKey.TENANT_ID)
                .append(")").append(_S_).append(DO_UPDATE_SET);

        int ci = columns.length;
        for (String column : columns) {
            ci--;
            if (column.equals(TxKey.ID) || column.equals(TxKey.TENANT_ID)) {
                continue;
            }
            sqlStatement.append(_S_).append(column).append(_S_).append(_EQ_)
                    .append(_S_).append(CASE)
                    .append(_S_).append(WHEN).append(_S_).append(EXCLUDED)
                    .append(".").append(column).append(_S_).append(IS_NOT_NULL)
                    .append(_S_).append(THEN).append(_S_).append(EXCLUDED)
                    .append(".").append(column).append(_S_).append(ELSE)
                    .append(_S_).append(JdbcTxDb.TABLE_NAME).append(".")
                    .append(column)
                    .append(_S_).append(END);
            if (ci != 0) {
                sqlStatement.append(",");
            }
        }
        sqlStatement.append(_END_);
        getDb().execute(sqlStatement.toString());
    }

    private static void applyFilter(StringBuilder sqlStatement, Filter filter) {
        if (filter != null) {
            validateFilter(filter);
            sqlStatement.append(_S_).append(WHERE).append(_S_)
                    .append(filter.getColumn());

            if (filter.getArgs().length == 1) {
                sqlStatement.append(_S_)
                        .append(FILTER_TO_OP.get(filter.getType())).append(
                                _S_)
                        .append(filter.getArgs()[0]);

            } else if (filter.getArgs().length > 1) {

                if (FilterType.EQ.equals(filter.getType()) ||
                        FilterType.LIKE.equals(filter.getType())) {

                    sqlStatement.append(_S_)
                            .append(IN)
                            .append(_S_)
                            .append("(")
                            .append(String.join(",",
                                    Arrays.stream(filter.getArgs())
                                            .map(Object::toString)
                                            .toArray(String[]::new)))
                            .append(")");
                } else {
                    throw new IllegalArgumentException(String.format(
                            "multiple arguments are not supported for filter " +
                                    "type <%s> on filter <%s>",
                            filter.getType(), filter.getColumn()));
                }

            } else {
                throw new IllegalArgumentException(String.format(
                        "filter for column <%s> requires at least 1 arg",
                        filter.getColumn()));
            }
        }
    }

    private static void validateFilter(Filter filter) {
        Objects.requireNonNull(filter.getColumn());
        if (filter.getColumn().isEmpty()) {
            throw new IllegalArgumentException(
                    String.format("filter <%s> is missing an column", filter));
        }
        Objects.requireNonNull(filter.getType());
    }
}
