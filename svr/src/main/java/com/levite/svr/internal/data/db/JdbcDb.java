package com.levite.svr.internal.data.db;

import com.levite.svr.internal.structs.Db;
import com.levite.svr.internal.structs.KeyValue;
import com.levite.svr.util.exceptions.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

/**
 * JDBC abstract implementation of {@link Db} that uses a {@link DataSource} in
 * order to execute SQL-based statements.
 * <p>
 * Concrete implementations that extend this are expected to provide a way of
 * creating the expected result of type {@param T} based on a given list of
 * {@link KeyValue} pairs. These results will be returned after the statement
 * is executed (if successful).
 *
 * @param <T> the expected result or return type from executing a statement.
 */
public abstract class JdbcDb<T> implements Db<T> {
    protected static final Logger logger =
            LoggerFactory.getLogger(JdbcDb.class);

    private interface GetResult {
        Object getResult(ResultSet result, Integer index) throws SQLException;
    }

    private static final Map<Class<?>, GetResult> TYPE_MAPPING =
            Map.of(Long.class, ResultSet::getLong,
                    Boolean.class, ResultSet::getBoolean,
                    String.class, ResultSet::getString,
                    LocalDate.class, ResultSet::getDate,
                    LocalTime.class, ResultSet::getTime);

    public static final Map<Integer, Class<?>> CLASS_MAPPING =
            Map.of(Types.BIGINT, Long.class,
                    Types.BOOLEAN, Boolean.class,
                    Types.VARCHAR, String.class,
                    Types.DATE, LocalDate.class,
                    Types.TIME, LocalTime.class);

    protected final DataSource dataSource;

    public JdbcDb(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Collection<T> execute(String statement) {
        Objects.requireNonNull(statement);
        try {
            final Connection conn = this.dataSource.getConnection();
            final ResultSet resultSet =
                    conn.createStatement().executeQuery(statement);

            final int columns = resultSet.getMetaData().getColumnCount();
            final ResultSetMetaData meta = resultSet.getMetaData();
            final List<T> results = new ArrayList<>();

            while (resultSet.next()) {
                final List<KeyValue> kvp = new ArrayList<>(columns);
                for (int i = 0; i < columns; i++) {
                    kvp.add(i, KeyValue.of(
                            meta.getColumnLabel(i),
                            TYPE_MAPPING.get(
                                            CLASS_MAPPING.get(meta.getColumnType(i)))
                                    .getResult(resultSet, i)));

                }
                results.add(create(kvp));
            }
            return Collections.unmodifiableList(results);

        } catch (SQLException e) {
            final String error = String.format(
                    "unable to execute statement <%s> because: %s", statement,
                    e.getMessage());
            logger.error(error, e);
            throw new DataAccessException(error, e);
        }
    }

    protected abstract T create(List<KeyValue> keyValues);
}
