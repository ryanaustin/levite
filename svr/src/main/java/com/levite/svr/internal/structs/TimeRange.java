package com.levite.svr.internal.structs;

import static com.levite.svr.util.CastingUtil.cast;

import java.util.Optional;

/**
 * Data structure that holds a start and end time to represent a range of time.
 */
public interface TimeRange {

    /**
     * Get the start of this time range.
     */
    Object getStart();

    /**
     * Get the end of this time range.
     */
    Object getEnd();

    /**
     * Get the start of this time range as the given type.  This only works if
     this {@link TimeRange} instance is able to {@link #convert convert the 
     held start time to the given class}, or the object is already an instance
     of the given class. 
     */
    default <T> T getStart(Class<T> type) {
        final Object start = getStart();
        return convert(start, type).orElseGet(() ->
                cast(start).to(type).orThrow(() -> {
                        throw new ClassCastException(String.format(
                                "start <%s> cannot be cast to type <%s>",
                                start, type));
        }));
    }

    /**
     * Get the end of this time range as the given type. This only works if
     this {@link TimeRange} instance is able to {@link #convert convert the 
     held end time to the given class}, or the object is already an instance
     of the given class. 
     */
    default <T> T getEnd(Class<T> type) {
        final Object start = getStart();

        return convert(start, type).orElseGet(() ->
                cast(start).to(type).orThrow(() -> {
                        throw new ClassCastException(String.format(
                                "start <%s> cannot be cast to type <%s>",
                                start, type));
        }));

    }

    /**
     * Converts the given time object to the given class, if this impl is able
     * to. If not, this should return an empty {@link Optional}, unless, any of
     * given arguments are null.
     */
    default <T> Optional<T> convert(Object time, Class<T> clazz) {
        return Optional.empty();
    }

}