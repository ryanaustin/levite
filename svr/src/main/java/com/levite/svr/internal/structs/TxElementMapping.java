package com.levite.svr.internal.structs;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Maps a column (element) name to a specific type.
 */
public class TxElementMapping
{
    private static final Map<String, TxElementMapping> TX_ELEMENT_MAP =
        new ConcurrentHashMap<>();

    private final String name;
    private final Class<?> type;

    private TxElementMapping(String name, Class<?> type) {
        if (TX_ELEMENT_MAP.containsKey(name)) {
            throw new IllegalStateException(
                "Tx element with name <" + name + "> already exists");
        }
        Objects.requireNonNull(name);
        Objects.requireNonNull(type);
        this.name = name;
        this.type = type;
        TX_ELEMENT_MAP.put(this.name, this);
    }

    public boolean isMatch(KeyValue keyValue) {
        return this.name.equals(keyValue.getKey()) && 
            (this.type.isInstance(keyValue.getValue()) ||
                    TxContext.get()
                            .getMapper()
                            .procure(keyValue, this.type).isPresent());
    }

    public static TxElementMapping create(String name, Class<?> type) {
        return new TxElementMapping(name, type);
    }

    public static TxElementMapping get(String name) {
        return TX_ELEMENT_MAP.get(name);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.name, this.type);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TxElementMapping)) return false;
        final TxElementMapping their = (TxElementMapping) obj;
        return Objects.equals(this.name, their.name) &&
            Objects.equals(this.type, their.type);
    }

}
