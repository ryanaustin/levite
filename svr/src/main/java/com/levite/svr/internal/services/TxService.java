package com.levite.svr.internal.services;

import com.levite.svr.internal.structs.Svc;
import com.levite.svr.internal.structs.Tx;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * Business logic service for handling Tx's.
 */
public interface TxService extends Svc {

    default <T> Collection<Tx> between(
        String tenantId, String txElementName, 
        T start, boolean includeStart, T end, boolean includeEnd) {

        noImpl("between");
        return Collections.emptyList();
    }

    default <T> Collection<Tx> betweenInclusive(
        String tenantId, String txElementName, T start, T end) {

        between(tenantId, txElementName, start, true, end, true);
        return Collections.emptyList();
    }

    default <T> Collection<Tx> betweenExclusive(
        String tenantId, String txElementName, T start, T end) {

        between(tenantId, txElementName, start, false, end, false);
        return Collections.emptyList();
    }

    default <T> Collection<Tx> above(
        String tenantId, String txElementName, T start) {
            
        noImpl("above");
        return Collections.emptyList();
    }

    default <T> Collection<Tx> aboveOrEqual(
        String tenantId, String txElementName, T start) {

        noImpl("aboveOrEqual");
        return Collections.emptyList();
    }

    default <T> Collection<Tx> below(
        String tenantId, String txElementName, T end) {

        noImpl("below");
        return Collections.emptyList();
    }

    default <T> Collection<Tx> belowOrEqual(
        String tenantId, String txElementName, T end) {

        noImpl("belowOrEqual");
        return Collections.emptyList();
    }

    default <T> Collection<Tx> equal(
        String tenantId, Map<String, Collection<T>> txElementsEqualTo) {
        noImpl("equal");
        return Collections.emptyList();
    }

    default <T> Collection<Tx> like(
        String tenantId, Map<String, Collection<T>> txElementsLike) {

        noImpl("like");
        return Collections.emptyList();
    }

    default void save(Collection<Tx> txs) {
        noImpl("save");
    }
}
