package com.levite.svr.internal.data.db;

import lombok.Setter;

import javax.sql.DataSource;

/**
 * Get a {@link DataSource} to access a DB.
 */
@Setter
public class DbDataSource {
    public static final String LEVITE_SQL_TYPE = "levite.sql.type";
    public static final String LEVITE_SQL_URL = "levite.sql.url";
    public static final String LEVITE_SQL_USERNAME = "levite.sql.username";
    public static final String LEVITE_SQL_PASSWORD = "levite.sql.password";
    public static final String LEVITE_SQL_DB = "levite.sql.db";
    public static final String LEVITE_SQL_DRIVER = "levite.sql.driver";
    public static final String SQL_URL_PREFIX = "://";

    protected String type;
    protected String url;
    protected String username;
    protected String password;
    protected String db;
    protected String driver;

    public DataSource getDataSource() {
        throw new UnsupportedOperationException(
                "getDataSource not implemented");
    }
}
