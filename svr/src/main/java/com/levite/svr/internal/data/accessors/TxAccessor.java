package com.levite.svr.internal.data.accessors;

import java.util.*;

import com.levite.svr.internal.structs.Accessor;
import com.levite.svr.internal.structs.Filter;
import com.levite.svr.internal.structs.Tx;

public interface TxAccessor extends Accessor
{
    default Collection<Tx> get(Filter filter) {
        return get(filter, new Filter [0]);
    }

    default Collection<Tx> get(Filter filter, Filter... moreFilters) {

        final List<Filter> filters = new ArrayList<>();
        filters.add(filter);
        filters.addAll(Arrays.asList(moreFilters));
        return get(filters);
    }

    default Collection<Tx> get(List<Filter> filters) {

        noImpl("get(filters)");
        return Collections.emptyList();
    }

    default void put(Tx tx, Tx... txs) {
        final List<Tx> l = new ArrayList<>();
        l.add(tx);
        l.addAll(Arrays.asList(txs));
        put(l);
    }

    default void put(Collection<Tx> txs) {
        noImpl("put(filters)");
    }

    default void remove(Filter filter, Filter... moreFilters) {
        final List<Filter> f = new ArrayList<>();
        f.add(filter);
        f.addAll(Arrays.asList(moreFilters));
        remove(f);
    }

    default void remove(Tx tx, Tx... txs) {
        final Set<Tx> r = new HashSet<>();
        r.add(tx);
        r.addAll(Arrays.asList(txs));
        remove(r);
    }

    default void remove(List<Filter> txs) {
        noImpl("get(filters)");
    }

    default void remove(Set<Tx> txs) {
        noImpl("remove(filters)");
    }

    default void purge(Filter filter, Filter... moreFilters) {
        final List<Filter> f = new ArrayList<>();
        f.add(filter);
        f.addAll(Arrays.asList(moreFilters));
        purge(f);
    }

    default void purge(List<Filter> filters) {
        noImpl("purge(filters)");
    }

    default void purge(Set<Tx> txs) {
        noImpl("purge(filters)");
    }

    default void purge(Tx tx, Tx... txs) {
        final Set<Tx> p = new HashSet<>();
        p.add(tx);
        p.addAll(Arrays.asList(txs));
        purge(p);
    }

}