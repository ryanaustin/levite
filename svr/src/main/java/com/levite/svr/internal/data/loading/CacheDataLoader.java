package com.levite.svr.internal.data.loading;

import java.io.InputStream;
import java.util.Collection;

import com.levite.svr.internal.data.cache.Cache;
import com.levite.svr.internal.structs.Tx;
import com.levite.svr.internal.structs.TxContext;
import com.levite.svr.util.StreamUtils;

/**
 * Loads data from input streams into the {@link Cache}.
 */
public class CacheDataLoader implements DataLoader {

    @Override
    public void load(DataType type, InputStream is) {
        final String json = StreamUtils.toJson(is);
        final Collection<Tx> txs = TxContext.get().getMapper().readTxs(json);

        txs.forEach(tx -> Cache.MEMORY.put(tx.getUniqueId(), tx));
    }
}
