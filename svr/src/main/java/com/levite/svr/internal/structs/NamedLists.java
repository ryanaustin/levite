package com.levite.svr.internal.structs;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Data structure that holds a map of lists, with each assigned a unique name.
 */
public class NamedLists<T> {
    private Map<String, List<T>> lists = new ConcurrentHashMap<>();

    public NamedLists() {}

    /**
     * Get the list mapped to the given name.
     */
    public List<T> getList(String listName) {
        return Collections.unmodifiableList(lists.get(listName));
    }

    /**
     * Add or replace a list with the given name.
     */
    public void add(String name, List<T> list) {
        lists.put(name, list);
    }
}

