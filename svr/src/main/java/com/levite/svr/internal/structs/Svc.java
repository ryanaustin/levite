package com.levite.svr.internal.structs;

import static com.levite.svr.util.ThreadCtx.get;

public interface Svc extends Base {
    
    default <A extends Accessor> A getAccessor(Class<A> accessor) {
        return get(accessor);
    }

}
