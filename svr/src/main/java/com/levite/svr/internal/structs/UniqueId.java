package com.levite.svr.internal.structs;

import lombok.Getter;

import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * Unique ID for a DB entry that is Tenant-based so with a unique string per
 * tenant.
 */
@Getter
public class UniqueId {
    public static final String TENANT_ID_NAME = "tenantId";
    public static final String ID_NAME = "id";

    private String tenantId;
    private String id;

    public UniqueId() {}

    public UniqueId(String tenantId) {
        Objects.requireNonNull(tenantId);
        this.tenantId = tenantId;
        this.id = UUID.randomUUID().toString();
    }

    public UniqueId(String tenantId, String id) {
        Objects.requireNonNull(tenantId);
        Objects.requireNonNull(id);
        this.tenantId = tenantId;
        this.id = id;
    }

    public static UniqueId of(String tenantId, String id) {
        return new UniqueId(tenantId, id);
    }

    public void setTenantId(String tenantId) {
        Objects.requireNonNull(id);
        this.tenantId = tenantId;
    }

    public void setId(String id) {
        Objects.requireNonNull(id);
        this.id = id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.tenantId, this.id);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof UniqueId)) return false;
        final UniqueId their = (UniqueId) obj;
        return Objects.equals(this.tenantId, their.tenantId) &&
            Objects.equals(this.id, their.id);
    }

    public static UniqueId getUniqueId(Map<Object, Object> entries) {
        if (!entries.containsKey(TENANT_ID_NAME) || !entries.containsKey(ID_NAME)) {
            throw new IllegalArgumentException(String.format(
                "UniqueId cannot be extracted from entries <%s> because " + 
                "both a <%s> and a <%s> are required", 
                TENANT_ID_NAME, ID_NAME));
        }
        return new UniqueId(
                entries.get(TENANT_ID_NAME).toString(),
                entries.get(ID_NAME).toString());
    }
}
