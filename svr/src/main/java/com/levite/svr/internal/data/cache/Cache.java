package com.levite.svr.internal.data.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.levite.svr.internal.structs.UniqueId;

public interface Cache {
    
    public static final Map<UniqueId, Object> MEMORY = 
            new ConcurrentHashMap<>();
            
}
