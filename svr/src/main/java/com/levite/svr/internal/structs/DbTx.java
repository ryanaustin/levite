package com.levite.svr.internal.structs;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

import com.levite.svr.util.CollectionUtils;

import lombok.Getter;

/**
 * Implementation of {@link Tx} that keeps a copy of the original key value
 * pairs. This item can be passed around and mutated. When {@link #getValues}
 * is called, all mutations are applied in the order that they were received.
 */
@Getter
public class DbTx implements Tx, Db {

    private final SortedSet<KeyValue> values = new ConcurrentSkipListSet<>();
    private final Map<TimedModification, KeyValue> mods = 
            new ConcurrentSkipListMap<>();
    private final Instant created;

    private enum Modification {
        REMOVE,
        PUT;

        public TimedModification now() {
            return new TimedModification(this);
        }
    }

    public static final class TimedModification
            implements Comparable<TimedModification> {
        private final Modification mod;
        private final Instant time;

        private TimedModification(Modification mod) {
            this.mod = mod;
            this.time = Instant.now();
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.mod, this.time);
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof TimedModification)) return false;
            final TimedModification their = (TimedModification) obj;
            return Objects.equals(this.time, their.time) &&
                Objects.equals(this.mod, their.mod);
        }

        @Override
        public int compareTo(TimedModification their) {
            return this.time.compareTo(their.time);
        }
    }

    private DbTx(Collection<KeyValue> keyValuePairs) {
        setValues(keyValuePairs);
        this.created = Instant.now();
    }

    public static DbTx from(Tx tx) {
        return new DbTx(tx.getValues());
    }

    public static DbTx create(Collection<KeyValue> keyValuePairs) {
        return new DbTx(keyValuePairs);
    }

    public void setValues(Collection<KeyValue> keyValuePairs) {
        this.values.clear();
        this.values.addAll (keyValuePairs);
    }

    @Override
    public Collection<KeyValue> getValues() {
        final Map<Object, Object> finalized = CollectionUtils.toMap(values);

        for (Map.Entry<TimedModification, KeyValue> mod : mods.entrySet()) {

            final KeyValue kv = mod.getValue();

            if (Modification.PUT.equals(mod.getKey().mod)) {
                finalized.put(kv.getKey(), kv.getValue());
            }
            else if (Modification.REMOVE.equals(mod.getKey().mod)) {
                finalized.remove(kv.getKey());
            }
        }

        return finalized
            .entrySet()
            .stream()
            .map(entry -> KeyValue.of(entry.getKey(), entry.getValue()))
            .collect(Collectors.toSet());
    }

    public Collection<KeyValue> getOriginalValues() {
        synchronized(values) {
            return Collections.unmodifiableSet(values);
        }
    }

    @Override
    public void put(Collection<KeyValue> keyValues) {
        synchronized (mods) {
            for (KeyValue kv : keyValues) {
                mods.put(Modification.PUT.now(), kv);
            }
        }
    }
    
    @Override
    public void remove(Collection<Object> keys) {
        synchronized (mods) {
            for (KeyValue kv : values) {
                if (keys.contains(kv.getKey())) {
                    mods.put(Modification.REMOVE.now(), kv);
                }
            }
        }
    }

    @Override
    public Tx save() {
        final Collection<KeyValue> temp = getValues();
        values.clear();
        values.addAll(temp);
        return this;
    }

}
