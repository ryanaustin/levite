package com.levite.svr.internal.structs;

import lombok.Getter;

import static com.levite.svr.internal.structs.UniqueId.ID_NAME;
import static com.levite.svr.internal.structs.UniqueId.TENANT_ID_NAME;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Key-value pair data structure.
 */
@Getter
public class KeyValue implements Comparable<KeyValue>
{
    private Object key = UUID.randomUUID().toString();
    private Object value;

    public KeyValue() {}
  
    public KeyValue(Object key, Object value) {
        this.key = key;
        this.value = value;
    }
    
    public static KeyValue of(Object key, Object value) {
        return new KeyValue(key, value);
    }
    
    public void setKey(Object key) {
        this.key = key;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.key, this.value);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof KeyValue)) return false;
        final KeyValue their = (KeyValue) obj;
        return Objects.equals(this.key, their.key) &&
            Objects.equals(this.value, their.value);
    }

    @Override
    public int compareTo(KeyValue their) {
        if (this.key != null) {
            if (their.key == null) return 1;
            final int keyCompare = this.key.toString()
                    .compareTo(their.key.toString());
            if (keyCompare != 0) return keyCompare;
        }
        if (this.value != null) {
            if (their.value == null) return 1;
            return this.value.toString()
                    .compareTo(their.value.toString());
        }
        return 0; // only if everything is null or both objects are equal.
    }

    public static Collection<KeyValue> asKeyValues(UniqueId uniqueId) {
        return List.of(
                KeyValue.of(TENANT_ID_NAME, uniqueId.getTenantId()),
                KeyValue.of(ID_NAME, uniqueId.getId()));
    }
}