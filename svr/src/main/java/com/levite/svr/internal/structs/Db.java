package com.levite.svr.internal.structs;

import java.util.Collection;

/**
 * Implementors must implement the {@link #execute(String)} for a String based
 * statement which should execute synchronously and return some collection of
 * results.
 *
 * @param <T> the type of return object to expect from the {@link #execute(String)}
 * statement.
 */
public interface Db<T> extends Base {
    /**
     * The name of this DB.
     */
    default String getName() {
        return this.getClass().getSimpleName();
    }

    /**
     * Executes the given String-based statement, then returns a collection of
     * results depending on this type of DB.
     *
     * @param statement the statement to execute.
     * @return a collection of some type of results.
     */
    default Collection<T> execute(String statement) {
        return noImpl("query(String statement)");
    }
}