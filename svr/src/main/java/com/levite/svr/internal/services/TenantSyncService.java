package com.levite.svr.internal.services;

import com.levite.svr.internal.structs.KeyValue;
import java.util.List;

/**
 * A service for a tenant that syncs what's necessary for that tenant.
 */
public interface TenantSyncService {
    /**
     * A sync name that describes what's being synced. The API requires for a
     * sync name to be provided so that all {@link TenantSyncService}s can be
     * injected and only appropriate ones can be executed on API calls.
     */
    String getSyncName();

    /**
     * Syncs everything that this sync service is responsible for syncing.
     *
     * @param tenantId the tenant that should be synced.
     * @return a list of results in key-value pair format.
     */
    List<KeyValue> syncAll(String tenantId);

    /**
     * Syncs <em>only</em> things that are identified by the given {@param syncId}.
     *
     * @param tenantId the tenant that should be synced.
     * @param syncId an id that singles out what should be synced.
     * @return a list of results in key-value pair format.
     */
    List<KeyValue> sync(String tenantId, Object syncId);
}
