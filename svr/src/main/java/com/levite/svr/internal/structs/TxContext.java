package com.levite.svr.internal.structs;

import com.levite.svr.internal.data.mutations.TxMutator;
import com.levite.svr.internal.data.mutations.TxNameAnonymizer;
import com.levite.svr.util.Mapper;
import com.levite.svr.util.ThreadCtx;

import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Context in which Tx's are handled.
 */
public interface TxContext {
    List<TxMutator> DEFAULT_MUTATORS = List.of(new TxNameAnonymizer());

    Principal getPrincipal();

    Collection<Permission> getPermissions();

    Object getRequest();

    Optional<Object> getTransaction();

    Mapper getMapper();

    List<TxMutator> getMutators();

    static TxContext get() {
        return ThreadCtx.get(TxContext.class);
    }
}
