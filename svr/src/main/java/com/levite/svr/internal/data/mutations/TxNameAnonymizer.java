package com.levite.svr.internal.data.mutations;

import com.levite.svr.internal.structs.NamedLists;
import com.levite.svr.internal.structs.Tx;
import com.levite.svr.internal.structs.TxContext;
import com.levite.svr.internal.structs.TxKey;
import com.levite.svr.util.NormalizedName;
import com.levite.svr.util.StreamUtils;
import com.levite.svr.util.aop.ServerClassLoader;
import lombok.Getter;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A {@link TxMutator} that anonymizes first and last names (if applicable) in a
 * pseudorandom manner.
 * <p>
 * The first and last name, combined legibly (i.e. "John Dear"), are hash
 * coded from a String to an Integer, then assigned an anonymous name that is
 * stored for a single day. 
 * <p>
 * This anonymizer guarantees no collision of different names as long as the
 * hash codes do not match. All names are converted to lower case before hashing
 * to ensure that this process is not case-sensitive and white space is removed.
 */
public class TxNameAnonymizer implements TxMutator {

    private static final String R_ANONYMOUS_NAMES = "anonymous-names.json";

    /**
     * Tracks anonymous name assignments.
     */
    private static final Map<Integer, AnonymousName> ASSIGNMENTS = 
            new ConcurrentHashMap<>();

    /**
     * Tracks anonymous names and the hash that they are stored under.
     */
    private static final Map<String, Integer> ANON_TO_HASH = 
            new ConcurrentHashMap<>();

    /**
     * Holds available anonymous names. 
     */
    private static final Stack<String> AVAILABLE_NAMES = new Stack<>();

    /**
     * Holds an anonymous name and its normalized name.
     */
    @Getter
    private static class AnonymousName {

        private final NormalizedName normalizedName;
        private final String anonymousName;
        
        private AnonymousName(NormalizedName name, String anonymousName) {
            this.normalizedName = name;
            this.anonymousName = anonymousName;
        }
    }

    @Override
    public Tx clean(Tx tx) {

        Objects.requireNonNull(tx, "Tx cannot be null");

        final Optional<NormalizedName> normalizedName = 
                Optional.ofNullable(NormalizedName.from(tx));
        
        if (normalizedName.isPresent()) {
            tx.remove(List.of(TxKey.NAME));

            // check if anonymous name was already assigned to hash code
            final Integer hashCode = 
                    normalizedName.get().getNormalized().hashCode();

            // get assigned or generate one
            final AnonymousName assigned = 
                    ASSIGNMENTS.computeIfAbsent(hashCode, code -> {

                final String anonymousName = 
                        getAnonymousName(normalizedName.get());
                
                ANON_TO_HASH.put(anonymousName, hashCode);
                return new AnonymousName(normalizedName.get(), anonymousName);
            });

            // put into tx
            tx.put(TxKey.ANON_NAME, assigned.getAnonymousName());
        }
        return tx;
    }

    /**
     * Get the assigned anonymous name for the given actual name, if one is 
     * actually assigned. Otherwise, this method returns an empty optional.
     * <p>
     * This method will throw an exception if the given actual name is null or
     * empty.
     */
    public static Optional<String> getAssignedAnonymousName(String actualName) {

        if (actualName.isBlank()) {
            throw new IllegalArgumentException("Actual name cannot be blank");
        }
        final String normalizedName = NormalizedName.normalize(actualName);
        return Optional.ofNullable(
                ASSIGNMENTS.get(normalizedName.hashCode()).getAnonymousName());
    }

    /**
     * Attempt to get the normalized name that is mapped to the given anonymous
     * name, if it is assigned. Returns an empty optional if the anonymous name
     * is not mapped.
     */
    public static Optional<NormalizedName> getNormalizedName(
        String anonymousName) {

        final Integer hashCode = ANON_TO_HASH.get(anonymousName);
        if (hashCode == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(
            ASSIGNMENTS.get(hashCode).getNormalizedName());
    }

    private static String getAnonymousName(NormalizedName normalizedName) {

        return AVAILABLE_NAMES.isEmpty() ?
                generateName() + " " + generateName() :
                AVAILABLE_NAMES.pop();
    }


    private static String generateName() {

        final String name = UUID.randomUUID().toString()
            .replaceAll("\\d", "")
            .replaceAll("-", "");
        return name.substring(0, 1).toUpperCase() + 
                name.substring(1);
    }

    /**
     * Clears all assignments and refetches anonymous names.
     */
    public static void reload(String mappedNameOfList) {
        ASSIGNMENTS.clear();
        ANON_TO_HASH.clear();

        try {
            final String json = StreamUtils.toJson(ServerClassLoader.get()
                .getResourceAsStream(R_ANONYMOUS_NAMES));

            final NamedLists<String> lists = TxContext.get().getMapper()
                    .readNamedLists(json, String.class);

            Optional.ofNullable(lists.getList(mappedNameOfList))
                    .ifPresent(AVAILABLE_NAMES::addAll);
        }
        catch (Exception e) {
            throw new IllegalStateException(String.format(
                "unable to load anonymous names from file <%s> because: %s",
                R_ANONYMOUS_NAMES, e.getMessage()), e);
        }
    }
}
