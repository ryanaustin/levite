package com.levite.svr.internal.services.impl;

import com.levite.svr.internal.data.accessors.TxAccessor;
import com.levite.svr.internal.data.mutations.TxMutator;
import com.levite.svr.internal.services.TxService;
import com.levite.svr.internal.structs.Filter;
import com.levite.svr.internal.structs.Filters;
import com.levite.svr.internal.structs.Tx;
import com.levite.svr.internal.structs.TxContext;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Implementation of {@link TxService} that converts request arguments to
 * {@link Filter filters} so that they can be handled by the accessor/data
 * layer.
 */
public abstract class TxServiceImpl implements TxService {

    @Override
    public <T> Collection<Tx> between(
        String tenantId, String txElementName, 
        T start, boolean includeStart, T end, boolean includeEnd) {

        final List<Filter> filters = new ArrayList<>();
        filters.add(Filters.TIME.create(
                includeStart ? Filter.FilterType.GTE : Filter.FilterType.GT,
                start));
        filters.add(Filters.TIME.create(
                includeEnd ? Filter.FilterType.LTE : Filter.FilterType.LT,
                end));
        final Collection<Tx> results = accessor().get(filters);

        final List<TxMutator> mutators = TxContext.get().getMutators();
        if (mutators == null  || mutators.isEmpty()) return results;
        final List<Tx> mutatedResults = new ArrayList<>();

        for (Tx result : results) {
            Tx mutated = result;
            for (TxMutator mutator : mutators) {
                mutated = mutator.clean(result);
            }
            mutatedResults.add(mutated);
        }
        return mutatedResults;
    }

    @Override
    public void save(Collection<Tx> txs) {
        accessor().put(txs);
    }

    protected abstract TxAccessor accessor();
}
