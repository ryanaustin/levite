package com.levite.svr.internal.structs;

import java.util.Objects;

import lombok.Getter;

/**
 * Allows for filtering on datasets for practically any library or data
 * access strategy.
 */
@Getter
public class Filter {

    private final String column;
    private final Object [] args;
    private final FilterType type;

    public enum FilterType {
        EQ,
        GT,
        LT,
        GTE,
        LTE,
        LIKE
    }

    private Filter(String column, Object arg, FilterType type) {
        this(column, new Object [] { arg }, type);
    }

    private Filter(String column, Object [] args, FilterType type) {
        Objects.requireNonNull(column);
        Objects.requireNonNull(args);
        if (column.isBlank()) {
            throw new IllegalArgumentException(
                "Filter column cannot be blank");
        }
        if (args.length == 0) {
            throw new IllegalArgumentException(
                "Filter args for column  <" + column + "> cannot be blank");
        }
        for (Object arg : args) {
            Objects.requireNonNull(arg);
        }

        this.column = column;
        this.args = args;
        this.type = type == null ? FilterType.EQ : type;
    }

    public static Filter create(
            String column, Object [] args, FilterType type) {
        return new Filter(column, args, type);
    }
    
    public static Filter create(String column, Object... args) {
        return new Filter(column, args, null);
    }

    public static Filter create(
            String column, FilterType type, Object... args) {
        return new Filter(column, args, type);
    }

}
