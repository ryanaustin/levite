package com.levite.svr.internal.data.db;

import com.levite.svr.internal.structs.DbTx;
import com.levite.svr.internal.structs.KeyValue;
import com.levite.svr.internal.structs.Tx;
import com.levite.svr.util.exceptions.DataAccessException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * JDBC implementation of {@link JdbcDb} for {@link Tx} entries.
 */
public class JdbcTxDb extends JdbcDb<Tx> implements TxDb {
    public static final String TABLE_NAME = "tx";

    public JdbcTxDb(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Tx create(List<KeyValue> keyValues) {
        return DbTx.create(keyValues);
    }

    @Override
    public Tx getSchema() {
        try {
            final List<KeyValue> keyValues = new ArrayList<>();
            final Connection conn = this.dataSource.getConnection();
            final ResultSet columns =
                    conn.getMetaData().getColumns(null, null, TABLE_NAME, "%");

            while (columns.next()) {
                final String columnName = columns.getString("COLUMN_NAME");
                final int datatype = columns.getInt("DATA_TYPE");
                keyValues.add(
                        KeyValue.of(columnName, CLASS_MAPPING.get(datatype)));
            }
            return DbTx.create(keyValues);
        } catch (SQLException e) {
            final String error = String.format(
                    "unable to get TX schema because: %s",
                    e.getMessage());
            logger.error(error, e);
            throw new DataAccessException(error, e);
        }
    }
}
