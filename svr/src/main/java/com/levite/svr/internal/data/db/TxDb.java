package com.levite.svr.internal.data.db;

import com.levite.svr.internal.structs.Db;
import com.levite.svr.internal.structs.Tx;
import com.levite.svr.internal.structs.UniqueId;

import java.util.Collection;

public interface TxDb extends Db<Tx> {

    default Collection<Tx> find(UniqueId uniqueId) {
        return noImpl("find(UniqueId)");
    }

    default void save(Tx tx, Tx... txs) {
        noImpl("save(Tx, Tx...)");
    }

    default void delete(UniqueId id, UniqueId... ids)  {
        noImpl("delete(UniqueId, UniqueId...)");
    }

    Tx getSchema();
}
