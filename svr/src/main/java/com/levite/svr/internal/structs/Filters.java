package com.levite.svr.internal.structs;

import lombok.Getter;

public class Filters {
    public static final FilterFactory TIME = new FilterFactory("time");

    @Getter
    public static final class FilterFactory {

        private final String key;

        public FilterFactory(String key) {
            this.key = key;
        }

        public Filter create(Filter.FilterType type, Object... args) {
            return Filter.create(key, type, args);
        }

    }
}
