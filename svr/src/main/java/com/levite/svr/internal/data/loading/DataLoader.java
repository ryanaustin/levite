package com.levite.svr.internal.data.loading;

import java.io.InputStream;

public interface DataLoader {
    
    enum DataType {
        TX
    }

    void load(DataType type, InputStream is);
}
