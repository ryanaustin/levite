package com.levite.svr.internal.structs;

/**
 * Indicates that an state object (or DB entry) belongs to a Tenant and has a 
 * unique identifier assigned to it.
 */
public interface Tenable {

    /**
     * Return a {@link UniqueId} for this tenable object.
     */
    UniqueId getUniqueId();

    /**
     * Return this tenable object's tenant's ID which is part of its {@link
     * UniqueId}.
     */
    default String getTenantId() {
        return getUniqueId().getTenantId();
    }

    /**
     * Return this tenable object's ID which is part of its {@link UniqueId}.
     */
    default String getId() {
        return getUniqueId().getId();
    }

}