package com.levite.svr.internal.structs;

public class TxKey {
    public static final String ID = "id";
    public static final String TENANT_ID = "tenantId";
    public static final String NAME = "name";
    public static final String ANON_NAME = "anonymousName";
    public static final String AMOUNT = "amount";
    public static final Object TYPE = "type";
    public static final Object DESCRIPTION = "description";
    public static final Object DATE = "date";
    public static final Object TIME = "time";
    public static final Object ACTIVE = "active";

    // hide
    private TxKey() {}
}
