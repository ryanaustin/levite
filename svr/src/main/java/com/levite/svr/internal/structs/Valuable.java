package com.levite.svr.internal.structs;

/**
 * Indicates that this object has inner values that can be retrieved with a key.
 */
public interface Valuable {

    /**
     * Returns an object for the given key or null if no such inner value exists.
     * @param keyOfValue a key pointing to the desired value.
     * @return possibly null.
     */
    Object get(Object keyOfValue);
}
