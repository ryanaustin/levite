package com.levite.svr.internal.data.mutations;

import com.levite.svr.internal.structs.Tx;

@FunctionalInterface
public interface TxMutator {
    
    Tx clean(Tx tx);

}
