package com.levite.svr.external.spring.di;

import com.levite.svr.external.stripe.StripeSyncByTenantService;
import com.levite.svr.external.stripe.StripeSyncDao;
import com.levite.svr.internal.services.TxService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AutowiredStripeSyncService extends StripeSyncByTenantService
        implements
        InitializingBean {

    private final StripeSyncDao stripeSyncDao;
    private  final TxService txService;

    @Autowired
    public AutowiredStripeSyncService(TxService txService, StripeSyncDao stripeSyncDao) {
        this.stripeSyncDao = stripeSyncDao;
        this.txService = txService;
    }

    @Override
    public void afterPropertiesSet() {
        init();
    }

    @Override
    protected StripeSyncDao getDao() {
        return stripeSyncDao;
    }

    @Override
    protected TxService getTxService() {
        return txService;
    }

    @Override
    public String getApiKey(String tenantId) {

        if ("redeemer-bible-church".equals(tenantId)) {
            return "sk_live_BnrPu7hAMQ35Dz6ubWJq2D9K00islL8kI3";
        }
        throw new UnsupportedOperationException(
                String.format("No API key exists for tenant id <%s>",
                        tenantId));
    }
}
