package com.levite.svr.external.spring.di;

import com.levite.svr.external.stripe.StripeSyncDaoImpl;
import com.levite.svr.internal.data.db.JdbcDb;
import com.levite.svr.internal.structs.Db;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AutowiredStripeSyncDao extends StripeSyncDaoImpl {

    private final JdbcDb<?> db;

    @Autowired
    public AutowiredStripeSyncDao(JdbcDb<?> db) {
        this.db = db;
    }

    @Override
    protected Db<?> getStripeSyncDb() {
        return db;
    }
}
