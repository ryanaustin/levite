package com.levite.svr.external.stripe;

import com.levite.svr.internal.structs.Db;
import com.levite.svr.internal.structs.KeyValue;
import com.levite.svr.util.SQL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

import static com.levite.svr.util.SQL.*;

public abstract class StripeSyncDaoImpl implements StripeSyncDao {
    protected static final Logger logger =
            LoggerFactory.getLogger(StripeSyncDaoImpl.class);
    public static final String TABLE_NAME = "stripe-sync";

    @Override
    public StripeSync addSync(StripeSync stripeSync) {
        stripeSync.getMetadata().put("timestamp", Instant.now());

        final StringBuilder sqlStatement = new StringBuilder();
        sqlStatement.append(INSERT_INTO).append(_S_).append(TABLE_NAME)
                .append("(");
        final List<KeyValue> schema = getSchema();
        final String[] columns =
                schema.stream().map(KeyValue::getKey).map(Object::toString)
                        .toArray(String[]::new);
        sqlStatement.append(String.join(",", columns)).append(")").append(_S_);
        SQL.appendValues(sqlStatement, columns, toSchemaMap(schema), List.of(stripeSync));
        sqlStatement.append(_END_);
        getStripeSyncDb().execute(sqlStatement.toString());
        return stripeSync;
    }

    private List<KeyValue> getSchema() {
        return List.of(
                KeyValue.of("id", String.class),
                KeyValue.of("tenantId", String.class),
                KeyValue.of("start", LocalDate.class),
                KeyValue.of("end", LocalDate.class),
                KeyValue.of("transactionCount", Integer.class),
                KeyValue.of("timestamp", Long.class),
                KeyValue.of("successful", Boolean.class),
                KeyValue.of("message", String.class));
    }

    protected abstract Db<?> getStripeSyncDb();
}
