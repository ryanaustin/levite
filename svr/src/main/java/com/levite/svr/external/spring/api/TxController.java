package com.levite.svr.external.spring.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.levite.svr.internal.services.TxService;
import com.levite.svr.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static com.levite.svr.util.Response.error;

@Controller
public class TxController 
{

    protected TxService txService;

    @Autowired
    public TxController(TxService txService) {
        this.txService = txService;
    }
    
    /**
     * Fetches Tx Items for a given tenantId within a date range.
     */
    @GetMapping("txs")
    @ResponseBody
    public Response getTxs(
        @RequestParam String tenantId,
        @RequestParam String startDate,
        @RequestParam String endDate
    )
    {
        try
        {
            return Response.done(
                this.txService.betweenInclusive(
                        tenantId,
                        "time",
                        parseDate(startDate),
                        parseDate(endDate))
            );
        }
        catch (JsonProcessingException e)
        {
            return error(
                    400,
                    "Unable to parse date range from " +
                            startDate + " to " + endDate);
        }
    }

    private static LocalDateTime parseDate(String date)
            throws JsonProcessingException
    {
        String [] parts = date.split("-");

        return LocalDateTime.of(
                LocalDate.of(
                    Integer.parseInt(parts[0]), // year
                    Integer.parseInt(parts[1]), // month
                    Integer.parseInt(parts[2])),
                LocalTime.of(0, 0, 0)); // day
    }

}
