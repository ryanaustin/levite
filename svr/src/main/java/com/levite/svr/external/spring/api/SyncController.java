package com.levite.svr.external.spring.api;

import com.levite.svr.internal.services.TenantSyncService;
import com.levite.svr.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Controller
public class SyncController {

    private final List<TenantSyncService> syncServices;

    @Autowired
    public SyncController(List<TenantSyncService> syncServices) {
        this.syncServices = syncServices;
    }

    @PutMapping("sync")
    @ResponseBody
    public Response syncNow(@RequestParam @NotNull String service,
            @RequestParam @NotNull String tenantId) {

        Optional<TenantSyncService> syncService = syncServices.stream()
                .filter(s -> service.equals(s.getSyncName()))
                .findFirst();

        if (syncService.isPresent()) {
            return Response.done(syncService.get().syncAll(tenantId));
        }
        return Response.done(String.format("service <%s> not found", service));
    }
}
