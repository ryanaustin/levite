package com.levite.svr.external.spring.di;

import com.levite.svr.internal.data.accessors.impl.SqlTxDbAccessor;
import com.levite.svr.internal.data.db.TxDb;
import com.levite.svr.internal.structs.Tx;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

@Component
public class AutowiredTxDbAccessor extends SqlTxDbAccessor implements
        InitializingBean {


    protected final TxDb txDb;
    private final Long schemaRefreshRate;
    private final AtomicReference<Tx> txSchema = new AtomicReference<>();
    private final ScheduledExecutorService executor =
            Executors.newScheduledThreadPool(1);

    @Autowired
    public AutowiredTxDbAccessor(TxDb txDb,
            @Value("levite.schema.refresh-rate.seconds:30") Long schemaRefreshRate) {
        this.txDb = txDb;
        this.schemaRefreshRate = schemaRefreshRate;
    }


    @Override
    protected TxDb getDb() {
        return txDb;
    }

    @Override
    protected Tx getSchema() {
        if (this.txSchema.get() == null) {
            this.txSchema.getAndSet(super.getSchema());
        }
        return txSchema.get();
    }

    @Override
    public void afterPropertiesSet() {
        this.executor
                .scheduleAtFixedRate(() -> txSchema.getAndSet(null),
                        schemaRefreshRate, schemaRefreshRate, TimeUnit.SECONDS);

    }
}
