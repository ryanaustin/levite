package com.levite.svr.external.spring;

import org.springframework.security.core.Authentication;

import com.levite.svr.internal.structs.Tenable;
import com.levite.svr.internal.structs.UniqueId;

/**
 * A User that is Authenticated within this Server's Context.
 */
public interface AuthenticatedUser 
    extends Authentication, Tenable
{

    @Override
    default UniqueId getUniqueId() {
        return UniqueId.of(resolveTenantId(), getName());
    }

    String resolveTenantId();
    
}
