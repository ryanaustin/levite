package com.levite.svr.external.spring.di;

import com.levite.svr.internal.data.db.JdbcDb;
import com.levite.svr.internal.structs.Db;
import com.levite.svr.internal.structs.KeyValue;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * A general Db that is not mapped to a specific table or value type.
 */
@Repository
public class AutowiredJdbcDb extends JdbcDb<Object> implements Db<Object> {

    public AutowiredJdbcDb(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Object create(List<KeyValue> keyValues) {
        return keyValues;
    }
}
