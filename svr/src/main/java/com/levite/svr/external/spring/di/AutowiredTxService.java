package com.levite.svr.external.spring.di;

import com.levite.svr.internal.data.accessors.TxAccessor;
import com.levite.svr.internal.services.impl.TxServiceImpl;
import com.levite.svr.util.aop.Contextualize;
import com.levite.svr.util.aop.Proxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Proxy(TxServiceImpl.class)
@Contextualize(TxAccessor.class)
public class AutowiredTxService extends TxServiceImpl
{
    
    protected final TxAccessor txAccessor;

    @Autowired
    public AutowiredTxService(TxAccessor txAccessor) {
        this.txAccessor = txAccessor;
    }

    @Override
    protected TxAccessor accessor() {
        return txAccessor;
    }
}
