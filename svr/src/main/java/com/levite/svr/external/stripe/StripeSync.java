package com.levite.svr.external.stripe;

import com.levite.svr.internal.structs.KeyValue;
import com.levite.svr.internal.structs.Tenable;
import com.levite.svr.internal.structs.UniqueId;
import com.levite.svr.internal.structs.Valuable;
import lombok.Getter;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Object that holds vital information regarding a stripe synchronization.
 */
@Getter
public class StripeSync implements Tenable, Valuable {
    public static final String END = "end";
    public static final String UUID_KEY = "id";
    public static final String TENANT_KEY = "tenantId";

    private final Map<Object, Object> metadata = new HashMap<>();

    private StripeSync(Collection<KeyValue> keyValues) {
        for (KeyValue kv : keyValues) {
            metadata.put(kv.getKey(), kv.getValue());
        }
        metadata.computeIfAbsent(UUID_KEY, k -> UUID.randomUUID().toString());
    }

    public static StripeSync of(Collection<KeyValue> keyValues) {
        return new StripeSync(keyValues);
    }

    public LocalDate getEnd() {
        Object obj = metadata.get(END);
        if (obj != null) {
            if (obj instanceof String) {
                return LocalDate.parse((String) obj);
            } else if (obj instanceof LocalDate) {
                return (LocalDate) obj;
            }
        }
        throw new IllegalStateException(
                "unable to get end date of sync because the value was missing");
    }

    @Override
    public UniqueId getUniqueId() {
        return UniqueId.of(metadata.get(TENANT_KEY).toString(),
                metadata.get(UUID_KEY).toString());
    }

    @Override
    public Object get(Object keyOfValue) {
        return metadata.get(keyOfValue);
    }
}
