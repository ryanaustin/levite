package com.levite.svr.external.jackson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.levite.svr.internal.structs.KeyValue;
import com.levite.svr.internal.structs.NamedLists;
import com.levite.svr.internal.structs.Tx;
import com.levite.svr.util.Mapper;
import com.levite.svr.util.exceptions.JsonParsingException;

import static com.levite.svr.external.jackson.JacksonTxUtil.asTxs;

public class JacksonMapper implements Mapper
{
    private static final ObjectMapper MAPPER = standard();

    protected static final Logger logger = 
            LoggerFactory.getLogger(JacksonTxUtil.class);

    public static <E> E read(String input, Class<E> clazz)
            throws JsonMappingException, JsonProcessingException {

        return MAPPER.readValue(input, clazz);
    }

    public static <E> E read(InputStream is, Class<E> clazz) 
            throws IOException {

        return MAPPER.readValue(is, clazz);
    }

    public static ObjectMapper standard() {

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }

    @Override
    public <T> Optional<T> procure(KeyValue keyValue, Class<T> type) {

        return Optional.empty();
    }

    @Override
    public <E> E readObject(String json, Class<E> clazz) {

        throw new UnsupportedOperationException(
            "Unimplemented method 'readObject'");
    }

    @Override
    public Collection<Tx> readTxs(String json) {

        try {
            return asTxs(read(json, ArrayNode.class));
        }
        catch (Exception e) {
            throw new JsonParsingException(String.format(
                "unable to parse JSON string <%s> into Txs because: %s",
                trimJson(json, 100), e.getMessage()), e);
        }
    }

    private static String trimJson(String json, int limit) {

        if (json.length() <= limit) return json;
        return json.substring(0, limit) + "... (trimmed)";
    }

    @Override
    public <T> NamedLists<T> readNamedLists(String json, Class<T> typeOfList) {

        try {
            final JsonNode node = read(json, JsonNode.class);
            final NamedLists<T> lists = new NamedLists<>();

            final Iterator<Map.Entry<String, JsonNode>> innerNodes = 
                    node.fields();
            
            while (innerNodes.hasNext()) {
                final Map.Entry<String, JsonNode> entry = innerNodes.next();

                if (!entry.getValue().isArray()) {
                    logger.warn(String.format(
                            "Entry with key <%s> does not contain a " + 
                            "list of %s", 
                            entry.getKey(), typeOfList.getSimpleName()));
                    continue;
                }

                final Iterator<JsonNode> elements = entry.getValue().elements();
                final List<T> list = new ArrayList<>();
                while (elements.hasNext()) {
                    list.add(MAPPER.convertValue(elements.next(), typeOfList));
                }

                if (list.isEmpty()) {
                    logger.warn(String.format(
                            "Entry with key <%s> contains an empty list " + 
                            "instead of a list with %s elements", 
                            entry.getKey(), typeOfList.getSimpleName()));
                    continue;
                }

                lists.add(entry.getKey(), list);
            }
            return lists;
        }
        catch (Exception e) {
            throw new JsonParsingException(String.format(
                "unable to parse JSON string <%s> into NamedList because: %s",
                trimJson(json, 100), e.getMessage()), e);
        }
    }

    @Override
    public String toJson(Object obj) {
        try {
            return MAPPER.writeValueAsString(obj);
        }
        catch (Exception e) {
            throw new JsonParsingException(String.format(
                "unable to parse <%s> into JSON string because: %s",
                obj, e.getMessage()), e);
        }
    }
}
