package com.levite.svr.external.web.security;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class AESEncryption implements EncryptionStrategy
{
    private static final String ALGORITHM = "AES/CBC/PKCS5Padding";
    private static final String IV = "77u-85r-012-buN6";

    private transient final SecretKey secretKey;
    private transient final IvParameterSpec iv;

    public AESEncryption(String secret) throws Exception
    {
        this.iv = generateIv(IV);
        this.secretKey = generateKey(secret);
    }
    
    public String encrypt(String input) 
    {
        try
        {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);
            byte[] cipherText = cipher.doFinal(input.getBytes());
            return Base64.getEncoder()
                .encodeToString(cipherText);
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("Unable to encrypt input because: " + e.getLocalizedMessage());
        }
    }

    public String decrypt(String cipherText)
    {
        try
        {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, secretKey, iv);
            byte[] plainText = cipher.doFinal(Base64.getDecoder()
                .decode(cipherText));
            return new String(plainText);
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("Unable to decrypt input because: " + e.getLocalizedMessage());
        }
    }

    /**
     * Generates a Secret Key using the given String
     */
    protected static SecretKey generateKey(String secret) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(secret.toCharArray(), secret.getBytes(), 65536, 256);
        SecretKey secretKey = new SecretKeySpec(keyFactory.generateSecret(spec).getEncoded(), "AES");
        return secretKey;
    }

    protected static IvParameterSpec generateIv(String iv) 
    {
        return new IvParameterSpec(iv.getBytes());
    }

}
