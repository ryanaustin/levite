package com.levite.svr.external.spring.di;

import com.levite.svr.internal.data.db.JdbcTxDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Profile("jdbc")
@Repository
public class AutowiredJdbcTxDb extends JdbcTxDb {
    @Autowired
    public AutowiredJdbcTxDb(DataSource dataSource) {
        super(dataSource);
    }
}
