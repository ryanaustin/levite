package com.levite.svr.external.stripe;

import java.time.LocalDate;

public interface StripeSyncService {

    StripeSync getLastSync(String tenantId);

    /**
     * Synchronizes a new {@link StripeSync} with the given tenant.
     *
     * @param tenantId the tenant's id.
     * @param start the start date of the sync.
     * @param end the end date of the sync.
     * @param numTransactions the number of transactions within the sync.
     * @return the stripe sync that was synchronized.
     */
    StripeSync syncNow(String tenantId, LocalDate start, LocalDate end, int numTransactions, boolean successful, String message);

    String getApiKey(String tenantId);
}
