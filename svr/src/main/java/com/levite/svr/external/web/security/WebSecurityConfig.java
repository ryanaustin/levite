package com.levite.svr.external.web.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter 
{
    protected static final Logger log = LoggerFactory.getLogger(WebSecurityConfig.class);

	@Override
    protected void configure(HttpSecurity http) throws Exception 
    {
        http
            .cors()
                .and()
            .logout()
                .clearAuthentication(true)
                .invalidateHttpSession(true)
                .logoutUrl("/player/logout")
                .logoutSuccessUrl("/logout")
                .permitAll()
                .and()
            .authorizeRequests()
                // Allow home page to be reached without 
                // requiring Players to be authenticated
                .antMatchers("/*").permitAll()
                .and()
            .authorizeRequests()
                .antMatchers("/user/**").authenticated()
                .and()
            .csrf()
            .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());

            log.info("Web Security Configuration Loaded");
    }

    @Bean
    public HttpFirewall allowUrlEncodedSlashHttpFirewall() 
    {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowUrlEncodedPercent(true);
        return firewall;
    }
    
}