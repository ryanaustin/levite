package com.levite.svr.external.web.security;

public interface EncryptionStrategy 
{
    public String encrypt(String input);
    public String decrypt(String encrypted);
}
