package com.levite.svr.external.spring.di;

import com.levite.svr.internal.data.db.DbDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

/**
 * Spring Data Source implementation.
 */
@Configuration
public class SpringDataSource extends DbDataSource {

    public SpringDataSource(
            @Value(LEVITE_SQL_TYPE) String type,
            @Value(LEVITE_SQL_URL) String url,
            @Value(LEVITE_SQL_USERNAME) String username,
            @Value(LEVITE_SQL_PASSWORD) String password,
            @Value(LEVITE_SQL_DB) String db,
            @Value(LEVITE_SQL_DRIVER) String driver
    ) {
        setType(type);
        setUrl(url);
        setUsername(username);
        setPassword(password);
        setDb(db);
        setDriver(driver);
    }

    @Bean
    public DataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(type + SQL_URL_PREFIX + url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

}
