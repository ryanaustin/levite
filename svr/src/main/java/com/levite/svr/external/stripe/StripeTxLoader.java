package com.levite.svr.external.stripe;

import com.levite.svr.internal.services.TxService;
import com.levite.svr.internal.structs.*;
import com.stripe.StripeClient;
import com.stripe.exception.StripeException;
import com.stripe.model.BalanceTransaction;
import com.stripe.model.Charge;
import com.stripe.model.StripeCollection;
import com.stripe.param.BalanceTransactionListParams;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import static java.lang.Math.abs;

/**
 * Loads transactions from Stripe and then sends them to the {@link TxService}.
 */
public class StripeTxLoader {
    private static final Long PAGING_LIMIT = 100L;
    private static final Logger logger =
            LoggerFactory.getLogger(StripeTxLoader.class);
    private static StripeClient client;
    private static final LocalTime syncTime = LocalTime.of(0, 0, 0);
    private static final ZoneOffset zoneOffset = ZoneOffset.UTC;
    private static final ChronoUnit syncInterval = ChronoUnit.MONTHS;

    /**
     * First, use the given {@link StripeSyncService} to load all stripe
     * transactions. Then, for all fetched transactions that are considered
     * valid and worth saving will be saved using the given {@link TxService}.
     *
     * @return A {@link StripeSync} object which holds metadata about how the sync
     * went. If a failure is encountered, the {@link StripeSync} will have a metadata
     * field explaining the error messsage.
     */
    public static StripeSync loadAndSave(StripeSyncService syncService,
            TxService txService, String tenantId) {
        final StripeClient stripe = getClient(tenantId);

        // defaults if no stripe sync exists or sync fails
        LocalDate syncStart = LocalDate.now().minus(1, syncInterval);
        LocalDate syncEnd = syncStart.plus(1, syncInterval);
        int numTxs = 0;

        final StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        try {
            stripe.issuing().transactions().list(null, null);

            final StripeSync lastStripeSync =
                    syncService.getLastSync(tenantId);
            syncStart = lastStripeSync.getEnd().plus(1, syncInterval);

            final Collection<Tx> txs = new ArrayList<>();
            String lastTransaction = null;
            boolean doneFetching = false;

            while (!doneFetching) {
                final BalanceTransactionListParams params =
                        BalanceTransactionListParams.builder()
                                .setLimit(PAGING_LIMIT)
                                .setStartingAfter(lastTransaction)
                                .setCreated(
                                        BalanceTransactionListParams.Created.builder()
                                                .setGt(syncStart.toEpochSecond(
                                                        syncTime,
                                                        zoneOffset))
                                                .build()).build();
                final StripeCollection<BalanceTransaction> transactions =
                        stripe.balanceTransactions().list(params);

                if (transactions.getData().size() < PAGING_LIMIT) {
                    doneFetching = true;
                }

                for (BalanceTransaction transaction : transactions.getData()) {
                    lastTransaction = transaction.getId();
                    if ("available".equals(transaction.getStatus())) {
                        final Charge charge = stripe.charges()
                                .retrieve(transaction.getSource());
                        if (charge != null) {
                            txs.add(convert(tenantId, zoneOffset, transaction,
                                    charge));
                        }
                    }
                }
            }
            txService.save(txs);
            numTxs = txs.size();
            stopWatch.stop();
        } catch (StripeException e) {
            logger.error(String.format(
                    "unable to get Stripe transactions because: %s",
                    e.getMessage()), e);
            stopWatch.stop();
            return syncService.syncNow(tenantId, syncStart, syncEnd, numTxs,
                    false, String.format("stripe sync failed in %s because: %s",
                            stopWatch.toSplitString(), e.getMessage()));
        }
        return syncService.syncNow(tenantId, syncStart, syncEnd, numTxs,
                true, String.format("done in %s", stopWatch.toSplitString()));
    }

    private static StripeClient getClient(String tenantId) {
        if (client == null) {
            try {
                client = new StripeClient(tenantId);
            } catch (Exception e) {
                logger.error(String.format(
                        "unable to initialize Stripe client because: %s",
                        e.getMessage()), e);
                throw new IllegalStateException(e);
            }
        }
        return client;
    }

    protected static Tx convert(String tenantId, ZoneOffset zoneOffset,
            BalanceTransaction transaction, Charge charge) {
        final Collection<KeyValue> keyValues = new ArrayList<>();
        keyValues.add(KeyValue.of(TxKey.ID, UUID.randomUUID().toString()));
        keyValues.add(KeyValue.of(TxKey.TENANT_ID, tenantId));
        keyValues.add(KeyValue.of(TxKey.AMOUNT, abs(transaction.getNet())));
        keyValues.add(KeyValue.of(TxKey.TYPE, getTxType(transaction.getNet())));
        keyValues.add(KeyValue.of(TxKey.DESCRIPTION,
                "stripe " + getTxType(transaction.getNet()) + " : " +
                        transaction.getDescription()));
        final LocalDateTime localDateTime =
                LocalDateTime.ofEpochSecond(transaction.getCreated(), 0,
                        zoneOffset);
        keyValues.add(KeyValue.of(TxKey.DATE, localDateTime.toLocalDate()));
        keyValues.add(KeyValue.of(TxKey.TIME, localDateTime.toLocalTime()));
        keyValues.add(KeyValue.of(TxKey.ACTIVE, true));
        keyValues.add(
                KeyValue.of(TxKey.NAME, charge.getBillingDetails().getName()));
        return DbTx.create(keyValues);
    }

    protected static TxType getTxType(long amount) {
        if (amount == 0L) {
            return TxType.OTHER;
        } else if (amount > 0L) {
            return TxType.GIFT;
        } else { // negative
            return TxType.EXPENSE;
        }
    }
}
