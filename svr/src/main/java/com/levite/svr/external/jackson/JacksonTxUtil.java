package com.levite.svr.external.jackson;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.levite.svr.internal.structs.DbTx;
import com.levite.svr.internal.structs.KeyValue;
import com.levite.svr.internal.structs.Tx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.levite.svr.util.CollectionUtils.streamOf;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public class JacksonTxUtil {
    protected static final Logger logger = LoggerFactory.getLogger(JacksonTxUtil.class);

    private JacksonTxUtil() {}

    public static Collection<Tx> asTxs(ArrayNode arrayNode) {
        final Collection<Tx> txs = new ArrayList<>();
        for (JsonNode node : arrayNode)
        {
            if (!node.fields().hasNext()) {
                logger.warn(String.format(
                    "JSON node cannot by converted into a TX because " +
                    "it has no fields. Node -> %s", node));
                continue;
            }

            txs.add(
                DbTx.create(
                    streamOf(node.fields())
                        .map(field -> KeyValue.of(
                            field.getKey(), asObject(field.getValue())))
                        .collect(Collectors.toList())));
        }
        return txs;
    }

    public static Object asObject(JsonNode jsonNode) {
        return jsonNode.isInt() ? jsonNode.asInt() :
            jsonNode.isBoolean() ? jsonNode.asBoolean() :
            jsonNode.isTextual() ? jsonNode.asText() :
            jsonNode.isLong() ? jsonNode.asLong() :
            jsonNode.isFloat() ? Float.parseFloat(jsonNode.asText()):
            jsonNode.isDouble() ? jsonNode.asDouble() :
            jsonNode.isBigDecimal() ? new BigDecimal(
                    jsonNode.asText(), MathContext.UNLIMITED) :
            jsonNode.isArray() ? streamOf(jsonNode.elements())
                .map(JacksonTxUtil::asObject)
                .collect(Collectors.toList()) :
            // must be a complex object with fields, so we will convert to
            // a key-value pair
                    !jsonNode.isEmpty() ? streamOf(jsonNode.fields())
                .map(field -> KeyValue.of(
                    field.getKey(), asObject(field.getValue())))
                .collect(Collectors.toList()) :
            // whelp, it is like that sometimes
            null;
    }
}
