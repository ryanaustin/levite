package com.levite.svr.external.spring.di;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.levite.svr.external.jackson.JacksonMapper;
import com.levite.svr.internal.data.db.TxDb;
import com.levite.svr.internal.structs.DbTx;
import com.levite.svr.internal.structs.Tx;
import com.levite.svr.internal.structs.UniqueId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static com.levite.svr.external.jackson.JacksonTxUtil.asTxs;
import static com.levite.svr.util.CastingUtil.cast;

/**
 * Spring autowired implementation of {@link }
 */
@Profile("in-memory")
@Repository
public class AutowiredInMemoryTxDb implements TxDb, InitializingBean
{
    protected static final Logger logger =
            LoggerFactory.getLogger(AutowiredInMemoryTxDb.class);

    private final Map<UniqueId, Tx> txDb;

    public AutowiredInMemoryTxDb()
    {
        this.txDb = new ConcurrentHashMap<>();
    }

    @Override
    public Collection<Tx> find(UniqueId uniqueId) 
    {
        return Collections.singletonList(this.txDb.get(uniqueId));
    }

    @Override
    public void save(Tx tx, Tx... txs) 
    {
        txDb.put(
            tx.getUniqueId(), 
            cast(tx).to(DbTx.class).orReturn(() -> DbTx.from(tx)));

        for (Tx txn : txs) {
            txDb.put(
                tx.getUniqueId(), 
                cast(txn).to(DbTx.class).orReturn(() -> DbTx.from(txn)));
        }
    }

    @Override
    public void delete(UniqueId id, UniqueId... ids) 
    {
        txDb.remove(id);
        for (UniqueId uniqueId : ids) {
            txDb.remove(uniqueId);
        }
    }

    @Override
    public Tx getSchema() {
        return DbTx.create(List.of());
    }

    @Override
    public void afterPropertiesSet() throws Exception 
    {
        // Create a test item
        final InputStream is = getClass().getClassLoader()
            .getResourceAsStream("data/budget-items.json");

        try (is) {
            Objects.requireNonNull(is);
            final Collection<Tx> txs = parseJSONFileIntoTxs(is);
            logger.info(String.format(
                    "Created {} Test Data within the In Memory DataBase",
                    txs.size()));
        }
    }

    private static Collection<Tx> parseJSONFileIntoTxs(InputStream is)
        throws IOException
    {
        return asTxs(JacksonMapper.read(is, ArrayNode.class));
    }    
}
