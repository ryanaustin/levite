package com.levite.svr.external.spring;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import lombok.Getter;

import java.util.Collection;

@Getter
public class AnonymousUser extends User {
    private String username;

    public AnonymousUser(
            Collection<? extends GrantedAuthority> authorities) {
        super(
                SecurityContextHolder.getContext()
                        .getAuthentication().getName(),
                "",
                authorities);
    }
}
