package com.levite.svr.external.stripe;

/**
 * A DAO for accessing {@link StripeSync} objects.
 */
public interface StripeSyncDao {
    /**
     * Add the given {@link StripeSync}, then return the actual {@link StripeSync}
     * that was saved (if additional fields were added).
     */
    StripeSync addSync(StripeSync stripeSync);
}
