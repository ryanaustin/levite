package com.levite.svr.external.stripe;

import com.levite.svr.internal.services.TenantSyncService;
import com.levite.svr.internal.services.TxService;
import com.levite.svr.internal.structs.KeyValue;
import com.levite.svr.util.exceptions.NotSupported;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public abstract class StripeSyncByTenantService implements StripeSyncService,
        TenantSyncService {

    public static final String STRIPE_SYNC_NAME = "stripe";

    @Override
    public StripeSync getLastSync(String tenantId) {
        return null;
    }

    @Override
    public StripeSync syncNow(String tenantId, LocalDate start, LocalDate end,
            int numTransactions, boolean successful, String message) {

        final StripeSync stripeSync = StripeSync.of(List.of(
                KeyValue.of("tenantId", tenantId),
                KeyValue.of("start", start),
                KeyValue.of("end", end),
                KeyValue.of("transactionCount", numTransactions),
                KeyValue.of("successful", successful),
                KeyValue.of("message", message)));

        return getDao().addSync(stripeSync);
    }

    @Override
    public String getSyncName() {
        return STRIPE_SYNC_NAME;
    }

    @Override
    public List<KeyValue> syncAll(String tenantId) {
        return StripeTxLoader.loadAndSave(this, getTxService(), tenantId).getMetadata()
                .entrySet()
                .stream()
                .map(e -> KeyValue.of(e.getKey(), e.getValue()))
                .collect(
                        Collectors.toList());
    }

    @Override
    public List<KeyValue> sync(String tenantId, Object syncId) {
        throw new NotSupported(String.format(
                "sync by ID is not supported at this time by <%s>",
                this.getClass().getSimpleName()), "42344651");
    }

    protected abstract StripeSyncDao getDao();

    protected abstract TxService getTxService();

    protected void init() {
    }
}
