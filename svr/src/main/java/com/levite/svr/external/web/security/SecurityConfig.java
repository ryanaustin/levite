package com.levite.svr.external.web.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Holds the Encryption Strategy for this Server
 */
@Configuration
public class SecurityConfig 
{

    @Bean
    public EncryptionStrategy encryptionStrategy(
        @Value("${budgeteer.server.key:apples-are-good-4u}") String secret
    ) throws Exception
    {
        return new AESEncryption(secret);
	}


}
