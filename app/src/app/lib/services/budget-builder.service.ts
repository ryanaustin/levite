import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Budget, BudgetCategory, BudgetItem, BudgetOptions, BudgetRange, BudgetRangeFormat, BudgetRangeSpec } from "../structs/budget";
import { MonthParser } from "../structs/month";
import { BudgetUtil } from "../util/budget-util";
import { BudgetClient } from "./budget-client.service";
import { UserProfileService } from "./user-profile.service";

@Injectable({ providedIn: 'root' })
export class BudgetBuilder
{
    /**
     * Whenever this is updated with a new Budget Range, the Budget API will be called
     * in order to update the Budget View.
     */
    private currentBudgetRange$ = new BehaviorSubject<BudgetRange>(null);

    private budgetItems = new Array<BudgetItem>();

    public budget: Budget;

    constructor(private budgetClient: BudgetClient, private userProfileService: UserProfileService)
    {
        this.subscribeToBudgetRangeChanges();
        this.subscribeToUserProfileUpdates();
        this.initBudget();
    }

    private subscribeToBudgetRangeChanges()
    {
        this.currentBudgetRange$.subscribe(
            (budgetRange) => 
            {
                if (!budgetRange) return;
                this.getLatestBudget(budgetRange);
            }
        );
    }

    private subscribeToUserProfileUpdates()
    {
        this.userProfileService.userProfileUpdated$.subscribe(
            (userProfile) => 
            {
                if (!userProfile) return;
                // For now, just update the budget as is whenever there's an update
                this.reloadBudget(this.calculateBudgetRange(userProfile.budgetOptions));
            }
        );
    }

    private initBudget()
    {
        this.budget = <Budget> {
            categories: new Map<string, BudgetCategory>()
        }
    }

    private calculateBudgetRange(options: BudgetOptions): BudgetRange
    {
        const rangeStart = options.lastRangeStart;

        // MONTH_RANGE by default
        let range = <BudgetRangeSpec> { 
            month: MonthParser.parse(Number(rangeStart.split('-')[1])), 
            year: Number(rangeStart.split('-')[0])
        };

        // When using DATE_RANGE instead
        if (options.budgetRangeFormat == BudgetRangeFormat.DATE_RANGE)
        {
            const startDate = BudgetUtil.parseDateFromString(rangeStart);
            range = {
                start: startDate,
                end: BudgetUtil.calculateEndDate(startDate, options.rangeLengthInDays)
            }
        }

        return new BudgetRange(options.budgetRangeFormat, range);
    }

    /**
     * Causes the Budget to be reloaded. If no budget range is provided, the current one 
     * will be reused.
     */
    public reloadBudget(budgetRange ?: BudgetRange)
    {
        if (!budgetRange) budgetRange = this.currentBudgetRange$.getValue();
        this.currentBudgetRange$.next(budgetRange);
    }

    private getLatestBudget(budgetRange: BudgetRange)
    {
        this.budgetClient.getBudget(budgetRange).subscribe(
            (response) => 
            {
                if (response.statusCode == 200) 
                {
                    this.budgetItems = <Array<BudgetItem>> response.message;
                    this.updateBudgetItems(this.budgetItems);
                }
                else
                {
                    console.error('Failed to get latest Budget because: ' + response.message);
                }
            }
        );
    }

    private updateBudgetItems(items: Array<BudgetItem>)
    {
        this.budget.categories = new Map<string, BudgetCategory>();

        // Organize items into their categories
        for (const item of items)
        {
            let category = this.budget.categories.get(item.category);

            // If category doesn't exist, create it
            if (!category)
            {
                category = { categoryName: item.category, items: new Array<BudgetItem>() };
                this.budget.categories.set(item.category, category);
            } 
            category.items.push(item);
        }
        console.log("Budget Updated: ", this.budgetItems, this.budget);
    }
}