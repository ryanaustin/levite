import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Month } from "../structs/month";
import { UserProfileService } from "./user-profile.service";
import { Response } from '../structs/response';
import { BudgetUtil } from "../util/budget-util";
import { BudgetRange, BudgetRangeFormat } from "../structs/budget";

const GET_BUDGET = "budget/?"

/**
 * This class will make the HTTP Calls to the Backend
 */
@Injectable({ providedIn: 'root' })
export class BudgetClient
{
    constructor(private http: HttpClient, private userProfileService: UserProfileService)
    {}

    public getBudgetBetweenDates(start: Date, end: Date)
    {
        const httpParams = new HttpParams()
            .set("username", this.userProfileService.userProfile.username)
            .set("start", BudgetUtil.parseDateToString(start))
            .set("end", BudgetUtil.parseDateToString(end));
            
        console.log("params", httpParams);

        return this.http.get<Response>(
            GET_BUDGET, { params: httpParams }
        );
    }

    public getBudgetForMonth(month: Month, year: number)
    {
        const start = new Date();
        start.setFullYear(year, Number(month) - 1, 1);

        const end = new Date(year, Number(month), 0);

        return this.getBudgetBetweenDates(start, end);
    }

    public getBudget(budgetRange: BudgetRange)
    {
        if (budgetRange.format == BudgetRangeFormat.DATE_RANGE)
        {
            return this.getBudgetBetweenDates(budgetRange.start, budgetRange.end);
        }
        else if (budgetRange.format == BudgetRangeFormat.MONTH_YEAR)
        {
            return this.getBudgetForMonth(budgetRange.month, budgetRange.year);
        }
        throw Error(`Budget Range Format ${budgetRange.format} not currently supported`);
    }
}