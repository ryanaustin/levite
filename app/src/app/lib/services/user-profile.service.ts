import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { BudgetRangeFormat } from "../structs/budget";
import { UserProfile } from "../structs/user-profile";

@Injectable({ providedIn: 'root' })
export class UserProfileService
{
    public userProfile: UserProfile = { 
        username: 'bobthebuilder', 
        budgetOptions: {
            budgetRangeFormat: BudgetRangeFormat.MONTH_YEAR,
            lastRangeStart: '2021-11-01',
            rangeLengthInDays: null
        }
    };

    public userProfileUpdated$ = new BehaviorSubject<UserProfile>(null);

    constructor()
    {}

    public initUserProfile()
    {
        this.userProfileUpdated$.next(this.userProfile);
    }
}