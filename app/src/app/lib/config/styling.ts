import { Injectable } from "@angular/core";

@Injectable({ providedIn: 'root' })
export class Styling 
{
    public theme: 'light' | 'dark';

    constructor()
    {
        this.theme = 'light';
    }
}