export class BudgetUtil
{
    public static parseDateFromString(str: string): Date
    {
        return new Date(Number(str.split('-')[0]), Number(str.split('-')[1]) - 1, Number(str.split('-')[2]));
    }


    public static parseDateToString(date: Date): string
    {
        return date.toISOString().split('T')[0];
    }

    public static calculateEndDate(start: Date, rangeLengthInDays: number): Date
    {
        const end = new Date(start);
        end.setDate(start.getDate() + rangeLengthInDays);
        return end;
    }
}