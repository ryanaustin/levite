export enum Month
{
    JANUARY = '01',
    FEBRUARY = '02',
    MARCH = '03',
    APRIL = '04',
    MAY = '05',
    JUNE = '06',
    JULY = '07',
    AUGUST = '08',
    SEPTEMBER = '09',
    OCTOBER = '10',
    NOVEMBER = '11',
    DECEMBER = '12'
}

export class MonthParser
{
    public static parse(number: number): Month
    {
        let str = number.toString();
        if (number < 10) str = '0' + number;
        return <Month> str;
    }
}