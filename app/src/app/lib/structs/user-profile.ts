import { BudgetOptions } from "./budget";

export interface UserProfile
{
    username: string,
    budgetOptions: BudgetOptions
}