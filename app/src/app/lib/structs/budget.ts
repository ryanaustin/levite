import { Month } from "./month";

export interface Budget 
{
    categories: Map<String, BudgetCategory>
}

export interface BudgetCategory
{
    categoryName: string,
    items: Array<BudgetItem>
}

export interface BudgetItem
{
    itemName: string,
    amount: number,
    category: string,
    date: string,
    type: string
}

export interface BudgetOptions
{
    budgetRangeFormat: BudgetRangeFormat,
    lastRangeStart: string,
    rangeLengthInDays: number
}

/**
 * Represents a Budget Range with values that can be used
 * for the API
 */
export class BudgetRange
{
    public format: BudgetRangeFormat;

    // For when format is DATE_RANGE
    public start: Date;
    public end: Date;

    // For when format is MONTH_YEAR
    public month: Month;
    public year: number;

    constructor(
        format: BudgetRangeFormat, 
        range: BudgetRangeSpec
    ) 
    {
        if (format == BudgetRangeFormat.DATE_RANGE)
        {
            this.start = range['start'];
            this.end = range['end'];
        }
        else if (format == BudgetRangeFormat.MONTH_YEAR)
        {
            this.month = range['month'];
            this.year = range['year'];
        }
        this.format = format;
    }
}

export enum BudgetRangeFormat
{
    DATE_RANGE = 'DATE_RANGE',
    MONTH_YEAR = 'MONTH_YEAR'
}

export type BudgetRangeSpec = { start: Date, end: Date } | { month: Month, year: number };