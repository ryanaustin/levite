export interface Response
{
  message: any,
  statusCode: number
}
