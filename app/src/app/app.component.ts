import { ChangeDetectorRef, Component, HostListener, NgZone, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Styling } from './lib/config/styling';
import { BudgetBuilder } from './lib/services/budget-builder.service';
import { UserProfileService } from './lib/services/user-profile.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit
{
  public title = 'EveryMove';

  public pgn: string = '';

  constructor(
    public dialog: MatDialog,
    private cd: ChangeDetectorRef,
    private zone: NgZone,
    public styling: Styling,
    public budgetBuilder: BudgetBuilder,
    public userProfileService: UserProfileService
  )
  {
  }

  public ngOnInit()
  {
    this.userProfileService.initUserProfile();
  }

}
