import { Component, Input } from "@angular/core";
import { startCase } from "lodash";
import { BudgetCategory } from "src/app/lib/structs/budget";

@Component({
    selector: 'category-display',
    template: `
    
    <div class="rounded category-display-container">
        <h3 class="rounded category-header">{{ this.categoryName() }}</h3>

        <ol class="category-items">
            <li class="clickable d-flex flex-row" *ngFor="let item of this.category.items">
                <div>{{ item.itemName }}</div>
                <div class="ml-auto">{{ this.toDollars(item.amount) }}</div>
            </li>
            <li class="clickable new-item">
                <fa-icon icon="plus" size="xs" class="new-item-color"></fa-icon>
                <span>New Item</span>
            </li>
        </ol>

        <div class="rounded category-footer">
            <span class="ml-auto">Total: {{ this.total() }}</span>
        </div>
    </div>
    
    `,
    styleUrls: ['./category-display.component.css']
})
export class CategoryDisplayComponent
{
    @Input() public category: BudgetCategory;

    constructor()
    {}

    public categoryName()
    {
        return startCase(this.category.categoryName);
    }

    public total(): string
    {
        let total = 0;
        for (const item of this.category.items)
        {
            total += item.amount;
        }
        return this.toDollars(total);
    }

    public toDollars(amount: number): string
    {
        if (amount % 100 == 0) return '$' + (amount/100).toLocaleString() + ".00";
        return '$' + (amount/100).toLocaleString();
    }
}