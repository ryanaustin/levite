import { Component } from "@angular/core";
import { BudgetBuilder } from "src/app/lib/services/budget-builder.service";

@Component({
    selector: 'budget-display',
    templateUrl: './budget-display.component.html',
    styleUrls: ['./budget-display.component.css']
})
export class BudgetDisplayComponent 
{
    constructor(public budgetBuilder: BudgetBuilder)
    {}
}