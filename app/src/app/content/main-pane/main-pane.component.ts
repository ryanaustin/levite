import { Component } from "@angular/core";

@Component({
    selector: 'main-pane',
    template: `
    
        <budget-display class="budget-display main-pane-display"></budget-display>
        <summary-display class="main-pane-display"></summary-display>

    `,
    styles: [`
    
        .budget-display {
            min-width: 65%;
        }

        .main-pane-display {
            padding: .25rem;
            height: 100%;
            flex-grow: 1;
        }

    `]
})
export class MainPaneComponent
{
    constructor()
    {}
}