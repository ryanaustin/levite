import { Component } from "@angular/core";
import { Styling } from "../lib/config/styling";

@Component({
    selector: 'content',
    template: `
        <section [ngClass]="styling.theme" class="rounded navigator-pane">
            <navigator-pane></navigator-pane>
        </section>

        <section [ngClass]="styling.theme" class="rounded main-pane">
            <main-pane class="main-pane-component h-100"></main-pane>
        </section>
    `,
    styleUrls: ['./content.component.css']
})
export class ContentComponent
{
    constructor(public styling: Styling)
    {}
}