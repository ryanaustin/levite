import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatRadioModule } from '@angular/material/radio';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { DndModule } from 'ngx-drag-drop';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppComponent } from './app.component';

import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';

import { ScrollingModule } from '@angular/cdk/scrolling';

import { DropdownModule } from 'primeng/dropdown';
import { SelectButtonModule } from 'primeng/selectbutton';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ButtonModule } from 'primeng/button';
import { SliderModule } from 'primeng/slider';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputTextModule } from 'primeng/inputtext';
import { InputNumberModule } from 'primeng/inputnumber';
import { Styling } from './lib/config/styling';
import { ContentComponent } from './content/content.component';
import { NavigatorPaneComponent } from './content/navigator-pane/navigator-pane.component';
import { MainPaneComponent } from './content/main-pane/main-pane.component';
import { BudgetDisplayComponent } from './content/main-pane/budget-display/budget-display.component';
import { CategoryDisplayComponent } from './content/main-pane/budget-display/category-display/category-display.component';
import { SummaryDisplayComponent } from './content/main-pane/summary-display/summary-display.component';
import { BudgetBuilder } from './lib/services/budget-builder.service';
import { BudgetClient } from './lib/services/budget-client.service';
import { UserProfileService } from './lib/services/user-profile.service';

@NgModule({
  declarations: [
    AppComponent,
    ContentComponent,
    NavigatorPaneComponent,
    MainPaneComponent,
    BudgetDisplayComponent,
    CategoryDisplayComponent,
    SummaryDisplayComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    NgbModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatRadioModule,
    MatDividerModule,
    MatListModule,
    ReactiveFormsModule,
    FormsModule,
    MatDialogModule,
    DragDropModule,
    DndModule,
    ReactiveFormsModule,
    MatInputModule,
    ScrollingModule,
    MatButtonModule,
    MatSelectModule,
    HttpClientModule,
    DropdownModule,
    SelectButtonModule,
    RadioButtonModule,
    ButtonModule,
    SliderModule,
    InputTextareaModule,
    InputTextModule,
    FontAwesomeModule,
    InputNumberModule,
  ],
  providers: [
    Styling,
    UserProfileService,
    BudgetBuilder,
    BudgetClient
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule {

  constructor(library: FaIconLibrary)
  {
    library.addIconPacks(fas, far);
  }
}
